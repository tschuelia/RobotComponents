/**

\page RobotComponents-Tutorials-debugdrawer Tutorial: Advanced Visualization Features - Drawing to the Debug Layer

In this tutorial, advanced visualization features are explained. It is shown how the DebugDrawer (and an extended version, the EntityDrawer) is used and how the layered visualization can be dispalyed in several ArmarXGui plugins.


\tableofcontents

\section RobotComponents-Tutorials-debugdrawer-gettingstarted Getting Started

For this tutorial, you need following prerequisites:
\li A copy of ArmarX including RobotAPI (containing the DebugDrawer component), ArmarXGui (to visualize the drawings), and optionally MemoryX (containing the EntityDrawer that can be used to display objects from the memory).


\subsection RobotComponents-Tutorials-debugdrawer-create-package Create your own package

If you already have a package in which you would like to add this tutorial, go to the next step \ref RobotComponents-Tutorials-debugdrawer-create-component.

In order to create a new ArmarX package called "RobotComponentTutorials", execute the following code in the directory where you want to make your package,
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init RobotComponentTutorials
\endcode
or you can use "-d" option to specify the directory.
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package init RobotComponentTutorials -d ${ArmarX_DIR}/
\endcode

\subsection RobotComponents-Tutorials-debugdrawer-create-component Generate template files of your new component and a test application
Execute the following commands in the toplevel directory of your package. All required files for the component and the test application will be generated.
\code{.sh}
cd RobotComponentTutorials
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package add component DebugDrawerTutorial
${ArmarX_DIR}/ArmarXCore/build/bin/armarx-package add application DebugDrawerTest
\endcode

Have a look into the directory to check if everything worked as intended. You should find the following files:
\code{.sh}
source/RobotComponentTutorials/components/CMakeLists.txt
source/RobotComponentTutorials/components/DebugDrawerTutorial
source/RobotComponentTutorials/components/DebugDrawerTutorial/DebugDrawerTutorial.h
source/RobotComponentTutorials/components/DebugDrawerTutorial/DebugDrawerTutorial.cpp
source/RobotComponentTutorials/components/DebugDrawerTutorial/CMakeLists.txt
\endcode


\section RobotComponents-Tutorials-debugdrawer-implementing Start Implementing
- First, we load the component to the QtCraetor IDE (as described in  \ref ArmarXCore-Tutorials-sce-edit-the-source-code "Tutorial: Counting with Statecharts -> Edit the source code"). You should have a project similar to the following setup:
<!--\htmlonly <img src="DebugDrawerProject.png" width="500px"> \endhtmlonly  -->
\image html DebugDrawerProject.png


- Add the following line into the CMakeLists.txt at the top-level directory of your project.
\code
depends_on_armarx_package(RobotAPI)
depends_on_armarx_package(MemoryX "OPTIONAL")
\endcode

- Edit the CMakeLists.txt file in your DebugDrawerTutorial component directory.
	-# Add the following dependencies
	\code
	find_package(Eigen3 QUIET)
	find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
	
	armarx_build_if(Eigen3_FOUND "Eigen3 not available")
	armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")
	
	include_directories(${Eigen3_INCLUDE_DIR})
	include_directories(${Simox_INCLUDE_DIRS})
	\endcode
	-# Add the library 
	\code
	set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore RobotAPICore ${Simox_LIBRARIES})
	\endcode
	
- Edit DebugDrawerTutorial.h
	-# Add includes
	\code
	#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
	#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
	\endcode
	-# Add DebugDrawer Proxy
	\code
	protected:
        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
	\endcode

- Edit DebugDrawerTutorial.cpp
	-# Offering the debugDrawer topic
	\code
	void DebugDrawerTutorial::onInitComponent()
	{
		// writing to the DebugDrawer topic. In ArmarX this topic's default name is DebugDrawerUpdates
		offeringTopic("DebugDrawerUpdates");
	}
	\endcode
	-# Get the proxy and draw a coordinate system
	\code
	void DebugDrawerTutorial::onConnectComponent()
	{
		// retrieve the proxy to the debug drawer
		debugDrawerPrx = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
		
		// setup a 4x4 homogeneous matrix
		Eigen::Matrix4f p;
		p.setIdentity();
		
		// set x,y,z coordinates (mm)
		p(0,3) = 0.0f;
		p(1,3) = 500.0f;
		p(2,3) = 1000.0f;
		
		// draw the pose
        PosePtr p_global(new Pose(p));
        debugDrawerPrx->setPoseDebugLayerVisu("MyPose", p_global);
	}
	\endcode

- Setup an application to test the component
        -# Update the application's /source/ddtest/applications/DebugDrawerTest/CMakeLists.txt
	\code
	find_package(Eigen3 QUIET)
	armarx_build_if(Eigen3_FOUND "Eigen3 not available")
	include_directories(${Eigen3_INCLUDE_DIR})
	set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore DebugDrawerTutorial)
	\endcode
	-# Update includes in the file DebugDrawerTestApp.h 
	\code
	#include <RobotComponentTutorials/components/DebugDrawerTutorial/DebugDrawerTutorial.h>
	\endcode
	-# Update the registration of the DebugDrawerTutorial component
	\code
	registry->addObject( armarx::Component::create<DebugDrawerTutorial>(properties) );
	\endcode
	At this point, the package should compile without any errors. To make sure it does so, first run CMake and then build the package.
	

\section RobotComponents-Tutorials-debugdrawer-results Show the DebugLayer in ArmarXGui 
The debug layer is implemented in several gui plugins (currently the DebugDrawerViewer, WorkingMemoryGui, RobotViewerGui) and in the Viewer of the DynamicSimulator.
In the following example, we show how to start the kinemtic simulation of Armar3 in order to show the debug layer of the RobotViewer gui plugin.
	-# First, we start a robot simulation (in a separate terminal shell), e.g. the RobotAPI scenario KinemticSimulationArmar3
	\code{.sh}
	cd ${ArmarX_DIR}/RobotAPI/scenarios/KinematicSimulationArmar3
	./startSceanrio.sh
	\endcode
	-# Now, we can start the ArmarXGui and load the RobotViewer
	\code{.sh}
	armarx gui
	\endcode
	Select AddWidget->RobotControl->RobotViewerGUI and configure as shown below.
	\image html RobotViewerConfig.png
	The gui plugin with some options and the Armar3 robot should be visualized as follows
	\image html RobotViewerArmar3.png
	-# The demo application is started in order to paint on the debug layer:
	\code{.sh}
	${ArmarX_DIR}/RobotComponentTutorials/build/bin/DebugDrawerTestAppRun 
	\endcode
	Then, the coordinate system should show up in the ArmarXGui 3D visualization as follows:
	\image html RobotViewerCoord.png
	-# Now you can safely stop the example by pressing <ctrl>-c in the shell where you started the DebugDrawerTestAppRun. The visualization remains until it is updated or the gui plugin is started again.
	
	
\section RobotComponents-Tutorials-debugdrawer-advaceddrawing Advanced Drawing Capabilities

Now we want to dispaly some more graphics. Therefore, we add periodic task to continously update the visualization.

	
- Edit DebugDrawerTutorial.h
	-# Add includes
	\code
	#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
	\endcode
	-# Add run method and periodic task object
	\code
	protected:
		void run();
        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
        PeriodicTask<DebugDrawerTutorial>::pointer_type task;
	\endcode
	
- Edit DebugDrawerTutorial.cpp

	-# Setup and start the periodic task
	\code
	void DebugDrawerTutorial::onConnectComponent()
	{
		// retrieve the proxy to the debug drawer and clear it 
		debugDrawerPrx = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
		debugDrawerPrx->clearDebugLayer();

		try
		{
			task = new PeriodicTask<DebugDrawerTutorial>(this, &DebugDrawerTutorial::run, 10, false, "DebugDrawerTutorialTask");
			task->start();
		}
		catch (...)
		{
			ARMARX_WARNING << "Failed to start DebugDrawerTutorial task";
		}
	}
	\endcode
        -# Implement periodic *run* method
	\code
	void DebugDrawerTutorial::run()
	{
		// setup a 4x4 homogeneous matrix
		Eigen::Matrix4f p;
		p.setIdentity();

		// set rotating coordinates (mm)
		p(0,3) = sin(clock() / 10000.0)*200;
		p(1,3) = 500.0f;
		p(2,3) = 1000.0f + cos(clock() / 10000.0)*200;
		PosePtr p_global(new Pose(p));

		// size of the box in mm
		Eigen::Vector3f v(100,100,100);
		Vector3Ptr s(new Vector3(v));

		// draw/update red box
		debugDrawerPrx->setBoxDebugLayerVisu("MyBox", p_global, s, DrawColor {1, 0, 0, 1});
	}
	\endcode
        -# Stop the *run()* method when the component gets disconnected:
        \code
        void DebugDrawerTutorial::onDisconnectComponent()
        {
            task->stop();
        }
        \endcode

	When starting the application again you should see a rotating box
	\image html RobotViewerBox.png
	-# Add some more visualizations
	The following list is an exerpt from the DebugDrawer's slice definition file (DebugDrawerInterface.ice) and shows several visualization features you can play around with.
		- Pose (Coordinate systems)
		\code
		setPoseDebugLayerVisu(string poseName, PoseBase globalPose);
		setScaledPoseDebugLayerVisu(string poseName, PoseBase globalPose, float scale);
		\endcode
		- Line
		\code
		void setLineDebugLayerVisu(string lineName, Vector3Base globalPosition1, Vector3Base globalPosition2, float lineWidth, DrawColor color);
		\endcode
		- Box
		\code
		void setBoxDebugLayerVisu(string boxName, PoseBase globalPose, Vector3Base dimensions, DrawColor color);
		\endcode
		- Text
		\code
		void setTextDebugLayerVisu(string textName, string text, Vector3Base globalPosition, DrawColor color, int size);
		\endcode
		- Sphere
		\code
		void setSphereDebugLayerVisu(string sphereName, Vector3Base globalPosition, DrawColor color, float radius);
		\endcode
		- PointCloud
		\code
		void setPointCloudDebugLayerVisu(string pointCloudName, DebugDrawerPointCloud pointCloud);
		\endcode
		- Polygon
		\code
		void setPolygonDebugLayerVisu(string polygonName, PolygonPointList polygonPoints, DrawColor colorInner, DrawColor colorBorder, float lineWidth);
		\endcode
		- Arrow
		\code
		void setArrowDebugLayerVisu(string arrowName, Vector3Base position, Vector3Base direction, DrawColor color, float length, float width);
		\endcode
		- Cylinder
		\code
		void setCylinderDebugLayerVisu(string cylinderName, Vector3Base globalPosition, Vector3Base direction, float length, float radius, DrawColor color);
		\endcode

- Custom Layers

        Until now, all visualizations have been drawn to the "debug" layer, which is a default layer of the DebugDrawer. You can add custom layers by specifying the layer string.
	E.g. the call
	\code
	debugDrawerPrx->setBoxDebugLayerVisu("MyBox", p_global, s, DrawColor {1, 0, 0, 1});
	\endcode
	becomes
	\code
	debugDrawerPrx->setBoxVisu("myLayer","MyBox", p_global, s, DrawColor {1, 0, 0, 1});
	\endcode
	You can clear your custom layer with
	\code
	clearLayer(string layerName);
	\endcode

- Add robot visualizations

	A robot can be visualized in a network transparent way by the DebugDrawer.
	Edit the DebugDrawerTutorial.cpp as follows:
		-# Setup the robot in onConnectComponent()
		\code
		void DebugDrawerTutorial::onConnectComponent()
		{
			...
			// draw robot collision model (ArmarIII.xml in project RobotAPI)
			debugDrawerPrx->setRobotVisu("MyRobotLayer", "MyRobot", "RobotAPI/robots/Armar3/ArmarIII.xml", "RobotAPI", armarx::CollisionModel);

			// set position of robot
			Eigen::Matrix4f p = Eigen::Matrix4f::Identity();
			p(0,3) = 1000;
			debugDrawerPrx->updateRobotPose("MyRobotLayer", "MyRobot", new Pose(p));

			// set color to green
			debugDrawerPrx->updateRobotColor("MyRobotLayer", "MyRobot", DrawColor {0, 1, 0, 1});
		}
		\endcode
		
		-# Update the configuration (joint angles) in the run() method
		\code
		void DebugDrawerTutorial::run()
		{
			...
			// update robot joint config
			std::map<std::string, float> c;
			c["Shoulder 1 R"] = -sin(clock() / 10000.0) / 2;
			c["Shoulder 1 L"] = sin(clock() / 10000.0) / 2;
			debugDrawerPrx->updateRobotConfig("MyRobotLayer", "MyRobot", c);
		}
		\endcode
		
		The result is a moving robot in the "MyRobotLayer" debug layer:
		\image html RobotViewerRobot.png
 

\section RobotComponents-Tutorials-debugdrawer-entitydrawer Showing Objects from Memory 

tbd... 
	

*/
