armarx_component_set_name(DMPComponentApp)

find_package(DMP QUIET)
armarx_build_if(DMP_FOUND "DMP not available")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if(DMP_FOUND AND Eigen3_FOUND)
    include_directories(${Eigen3_INCLUDE_DIR} ${DMP_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS DMPComponent)

set(SOURCES main.cpp)

armarx_add_component_executable("${SOURCES}")
