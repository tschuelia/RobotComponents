armarx_component_set_name(MotionPlanningRemoteObjectNodeApp)

find_package(Eigen3 REQUIRED)
include_directories(${Eigen3_INCLUDE_DIR})

set(COMPONENT_LIBS MotionPlanning)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")

