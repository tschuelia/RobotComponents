/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::GraspingManagerTest
 * @author     Valerij Wittenbeck ( valerij dot wittenbeck at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_MANAGEDICEOBJECT_RobotComponents_GraspingManagerTest_H
#define _ARMARX_MANAGEDICEOBJECT_RobotComponents_GraspingManagerTest_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/interface/component/WorkingMemoryInterface.h>
#include <MemoryX/interface/component/PriorKnowledgeInterface.h>
#include <RobotComponents/components/GraspingManager/GraspingManager.h>
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <MemoryX/libraries/motionmodels/MotionModelStaticObject.h>

namespace armarx
{
    class GraspingManagerTestPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GraspingManagerTestPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("objectToGrasp", "", "the object for which the grasp should be planned");
        }
    };

    /**
     * @class GraspingManagerTest
     * @brief A brief description
     *
     * Detailed Description
     */
    class GraspingManagerTest :
        virtual public armarx::ManagedIceObject,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "GraspingManagerTest";
        }

        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return armarx::PropertyDefinitionsPtr(new GraspingManagerTestPropertyDefinitions(
                    getConfigIdentifier()));
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent()
        {
            usingProxy("GraspingManager");
            usingProxy("WorkingMemory");
            usingProxy("PriorKnowledge");
            usingProxy("RobotStateComponent");
        }

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent()
        {
            std::string objectName = getProperty<std::string>("objectToGrasp").getValue();
            wm = getProxy<memoryx::WorkingMemoryInterfacePrx>("WorkingMemory");
            RobotStateComponentInterfacePrx robot = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent");
            auto object = wm->getObjectInstancesSegment()->getEntityByName(objectName);
            if (!object)
            {
                Eigen::Quaternionf q(
                    0.5174515247344971,
                    -0.5286158323287964,
                    0.4808708131313324,
                    -0.4707148373126984);
                wm->getObjectInstancesSegment()->addObjectInstance(objectName, objectName,
                        new LinkedPose(q.toRotationMatrix(),
                                       Eigen::Vector3f(4130.22705078125,
                                                       7142.34765625,
                                                       1170.102416992188), GlobalFrame, robot->getSynchronizedRobot()),
                        new memoryx::MotionModelStaticObject(robot));
            }
            prior = getProxy<memoryx::PriorKnowledgeInterfacePrx>("PriorKnowledge");
            assignProxy(gm, "GraspingManager");

            ARMARX_INFO << "initializing object";
            object = wm->getObjectInstancesSegment()->getEntityByName(objectName);
            ARMARX_CHECK_EXPRESSION(object);

            ARMARX_INFO << "object initialized, generating grasping trajectory";
            auto result = gm->generateGraspingTrajectory(object->getId());

            //            ARMARX_INFO << "Got result:";
            //            ARMARX_INFO << "=== Pose Trajectory ===";
            //            for (const RobotPoseTrajectoryPoint& ptp : result.poseTrajectory)
            //            {
            //                ARMARX_INFO << "ts " << ptp.timeStamp << ": ";
            //                ARMARX_INFO << VAROUT(*FramedPosePtr::dynamicCast(ptp.robotPose));
            //            }

            //            ARMARX_INFO << "=== Joint Trajectory ===";
            //            for (const RobotConfigTrajectoryPoint& rcpt : result.configTrajectory)
            //            {
            //                ARMARX_INFO << "ts " << rcpt.timeStamp << ": ";
            //                ARMARX_INFO << rcpt.jointConfig;
            //            }
        }

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent()
        {

        }

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent()
        {

        }

    private:
        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::WorkingMemoryInterfacePrx wm;
        GraspingManagerInterfacePrx gm;
    };
}

#endif
