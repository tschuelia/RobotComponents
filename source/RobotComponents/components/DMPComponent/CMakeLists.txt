armarx_component_set_name("DMPComponent")

find_package(DMP QUIET)
armarx_build_if(DMP_FOUND "DMP not available")

find_package(MMMCore QUIET)
armarx_build_if(MMMCore_FOUND "MMMCore not available")

find_package(MMMTools QUIET)
armarx_build_if(MMMTools_FOUND "MMMTOOLS not available")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

find_package(IVT QUIET)
armarx_build_if(IVT_FOUND "IVT not available")

armarx_build_if(MemoryX_FOUND "MemoryX not available")


# all include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
if(DMP_FOUND AND MMMCore_FOUND AND Eigen3_FOUND AND MMMTools_FOUND)
    message(STATUS "including and linking stuff: ${DMP_LIB_DIRS}")

    include_directories(${Eigen3_INCLUDE_DIR} ${DMP_INCLUDE_DIRS})
    link_directories(${IVT_LIBRARY_DIRS} ${DMP_LIB_DIRS})
endif()

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MemoryXMemoryTypes ${DMP_LIBRARIES} MMMCore RobotComponentsInterfaces)

set(SOURCES
./DMPInstance.cpp
./DMPComponent.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)
set(HEADERS
./DMPInstance.h
./DMPComponent.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
