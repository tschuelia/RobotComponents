/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DMPComponent
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotAPI_DMPComponent_H
#define _ARMARX_COMPONENT_RobotAPI_DMPComponent_H



#include <RobotComponents/interface/components/DMPComponentBase.h>
#include <MemoryX/interface/component/WorkingMemoryInterface.h>
#include <MemoryX/interface/component/LongtermMemoryInterface.h>
#include <MemoryX/libraries/memorytypes/entity/DMPEntity.h>

#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>


#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

#include <boost/filesystem.hpp>


#include <ArmarXCore/core/Component.h>
//#include "dmp/representation/dmp/dmpinterface.h"
//#include "dmp/representation/dmp/basicdmp.h"
//#include "dmp/representation/dmp/dmp3rdorder.h"
//#include "dmp/representation/dmp/quaterniondmp.h"
//#include "dmp/representation/dmp/endvelodmp.h"
//#include "dmp/representation/dmp/dmp3rdorderforcefield.h"
//#include "dmp/representation/dmp/forcefielddmp.h"
//#include "dmp/representation/dmp/adaptive3rdorderdmp.h"
//#include "dmp/representation/dmp/simpleendvelodmp.h"
////#include "dmp/representation/dmp/endveloforcefielddmp.h"
////#include "dmp/representation/dmp/endveloforcefieldwithobjrepulsiondmp.h"
//#include "dmp/representation/dmp/periodictransientdmp.h"

#include "DMPInstance.h"

namespace armarx
{
    /**
     * @class DMPComponentPropertyDefinitions
     * @brief
     */


    class DMPComponentPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DMPComponentPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //            defineRequiredProperty<std::string>("LongtermMemoryName", "Description");
            defineOptionalProperty<std::string>("LongtermMemoryName", "LongtermMemory", "Name of the LongtermMemory component");
        }
    };

    /**
     * @defgroup Component-DMPComponent DMPComponent
     * @ingroup RobotComponents-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */

    typedef std::pair<DMPInstancePtr, DMPInstanceBasePrx > DMPInstancePair;
    typedef std::pair<std::string, std::pair<DMPInstancePtr, DMPInstanceBasePrx> > DMPPair;
    typedef std::map<std::string, std::pair<DMPInstancePtr, DMPInstanceBasePrx> > DMPMap;

    /**
     * @ingroup Component-DMPComponent
     * @brief The DMPComponent class
     */
    class DMPComponent :
        virtual public Component,
        virtual public DMPComponentBase
    {
    public:

        DMPComponent():
            ctime(0.0),
            timestep(0.001)
        {

        }

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "DMPComponent";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return armarx::PropertyDefinitionsPtr
            {
                new DMPComponentPropertyDefinitions{getConfigIdentifier()}
            };
        }

        virtual DMPInstanceBasePrx getDMP(const std::string& dmpName, const Ice::Current& = ::Ice::Current());

        // DMP Database related
        virtual DMPInstanceBasePrx getDMPFromDatabase(const std::string& dmpEntityName, const std::string& dmpName = "UNKNOWN", const Ice::Current& = ::Ice::Current());
        virtual DMPInstanceBasePrx getDMPFromDatabaseById(const std::string& dbId, const Ice::Current&);
        virtual DMPInstanceBasePrx getDMPFromFile(const std::string& fileName, const std::string& dmpName = "UNKNOWN", const Ice::Current& = ::Ice::Current());

        virtual void storeDMPInFile(const std::string& fileName, const std::string& dmpName, const Ice::Current&);
        virtual void storeDMPInDatabase(const std::string& dmpName, const std::string& name, const ::Ice::Current& = ::Ice::Current());
        virtual void removeDMPFromDatabase(const std::string& name, const ::Ice::Current& = ::Ice::Current());
        void removeDMPFromDatabaseById(const std::string& dbId, const Ice::Current&);


        //transmit data from client to server (using ice)
        virtual DMPInstanceBasePrx instantiateDMP(const std::string& dmpName, const std::string& DMPType, int kernelSize, const ::Ice::Current& = ::Ice::Current());

        virtual void setDMPState(const std::string& dmpName, const ::armarx::cStateVec& state, const ::Ice::Current& = ::Ice::Current());
        virtual void setGoal(const std::string& dmpName, const Ice::DoubleSeq& value, const Ice::Current& = ::Ice::Current());
        virtual void setStartPosition(const std::string& dmpName, const Ice::DoubleSeq& value, const Ice::Current& = ::Ice::Current());
        virtual void setCanonicalValues(const std::string& dmpName, const Ice::DoubleSeq& value, const Ice::Current& = ::Ice::Current());

        virtual void readTrajectoryFromFile(const std::string& dmpName, const std::string& file, double times = 1, const ::Ice::Current& = ::Ice::Current());

        virtual void trainDMP(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual void setAmpl(const std::string& dmpName, int dim, double value, const ::Ice::Current& = ::Ice::Current());
        virtual double getAmpl(const std::string& dmpName, int dim, const ::Ice::Current& = ::Ice::Current());


        virtual double getTemporalFactor(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual void setTemporalFactor(const std::string& dmpName, double tau, const ::Ice::Current& = ::Ice::Current());


        virtual Vec2D calcTrajectory(const std::string& dmpName, double startTime, double timeStep, double endTime,
                                     const ::Ice::DoubleSeq& goal,
                                     const cStateVec& states,
                                     const ::Ice::DoubleSeq& canonicalValues, double temporalFactor, const ::Ice::Current& = ::Ice::Current());

        // time manager
        virtual double getTimeStep(const ::Ice::Current& = ::Ice::Current())
        {
            return timestep;
        }

        virtual void setTimeStep(double ts, const ::Ice::Current& = ::Ice::Current());

        virtual double getCurrentTime(const ::Ice::Current& = ::Ice::Current());

        virtual void resetTime(const ::Ice::Current& = ::Ice::Current());

        virtual void resetCanonicalValues(const ::Ice::Current& = ::Ice::Current());


        virtual double getDampingFactor(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());

        virtual double getSpringFactor(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());

        virtual double getForceTerm(const std::string& dmpName, const Ice::DoubleSeq& canonicalValues, int dim, const Ice::Current& = ::Ice::Current());

        //transmit data from server to client (using ice)
        virtual cStateVec getNextState(const std::string& dmpName, const cStateVec& states, const ::Ice::Current& = ::Ice::Current());
        virtual cStateVec getCurrentState(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual Ice::DoubleSeq getCanonicalValues(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual Ice::DoubleSeq getTrajGoal(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual ::armarx::cStateVec getTrajStartState(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());

        virtual std::string getDMPType(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());

        virtual Ice::DoubleSeq getStartPosition(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());

        virtual SVector getDMPNameList(const ::Ice::Current& = ::Ice::Current());

        virtual void eraseDMP(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual bool isDMPExist(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());
        virtual int getDMPDim(const std::string& dmpName, const ::Ice::Current& = ::Ice::Current());


        double ctime;
        double timestep;
        std::vector<int> usedDimensions;

    private:
        DMPInstanceBasePrx findInstancePrx(std::string name);
        DMPInstancePtr findInstancePtr(std::string name);
        DMPInstanceBasePrx createDMPInstancePrx(DMPInstancePtr dmpPtr);

    protected:
        DMPMap dmpPool;

        memoryx::LongtermMemoryInterfacePrx longtermMemoryPrx;
        memoryx::PersistentDMPDataSegmentBasePrx dmpDataMemoryPrx;

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        //        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();
    };
    typedef ::IceInternal::Handle< ::armarx::DMPComponent> DMPComponentPtr;
}



#endif
