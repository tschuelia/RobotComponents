//
//-------------------------------------------------------------
//
//  ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//  Copyright
//  Universite catholique de Louvain
//  Departement de Mecanique
//  Unite de Production Mecanique et Machines
//  2, Place du Levant
//  1348 Louvain-la-Neuve
//  http://www.robotran.be//
//
//  ==> Generation Date : Sun Oct 16 17:35:45 2016
//
//  ==> Project name : ArmarIV-GazeStab-virt
//  ==> using XML input file
//
//  ==> Number of joints : 16
//
//  ==> Function : F 6 : Sensors Kinematical Informations (sens)
//  ==> Flops complexity : 1020
//
//  ==> Generation Time :  0.020 seconds
//  ==> Post-Processing :  0.010 seconds
//
//-------------------------------------------------------------
//

#include <math.h>

//#include "mbs_data.h"
//#include "mbs_project_interface.h"
#include "user_hard_param_WO_torso.h"
#include "mbs_sensor2.h"

void  mbs_sensor_ArmarIV_GazeStab_virt(MbsSensor* sens,
                                       double* q,
                                       double* qd,
                                       double* qdd,
                                       int isens)
{

#include "mbs_sensor_ArmarIV-GazeStab-virt.h"

    //double *q = s->q;
    //double *qd = s->qd;
    //double *qdd = s->qdd;

    //double **frc = s->frc;
    //double **trq = s->trq;



    // === begin imp_aux ===

    // === end imp_aux ===

    // ===== BEGIN task 0 =====

    // Sensor Kinematics



    // = = Block_0_0_0_0_0_1 = =

    // Trigonometric Variables

    C4 = cos(q[4]);
    S4 = sin(q[4]);
    C5 = cos(q[5]);
    S5 = sin(q[5]);
    C6 = cos(q[6]);
    S6 = sin(q[6]);
    C7 = cos(q[7]);
    S7 = sin(q[7]);
    C8 = cos(q[8]);
    S8 = sin(q[8]);
    C9 = cos(q[9]);
    S9 = sin(q[9]);
    C10 = cos(q[10]);
    S10 = sin(q[10]);
    C11 = cos(q[11]);
    S11 = sin(q[11]);
    C12 = cos(q[12]);
    S12 = sin(q[12]);
    C13 = cos(q[13]);
    S13 = sin(q[13]);
    C15 = cos(q[15]);
    S15 = sin(q[15]);
    C16 = cos(q[16]);
    S16 = sin(q[16]);

    // ====== END Task 0 ======

    // ===== BEGIN task 1 =====

    switch (isens)
    {
 
        case 1:
 


            // = = Block_1_0_0_1_0_1 = =

            // Sensor Kinematics


            ROcp0_25 = S4 * S5;
            ROcp0_35 = -C4 * S5;
            ROcp0_85 = -S4 * C5;
            ROcp0_95 = C4 * C5;
            ROcp0_16 = C5 * C6;
            ROcp0_26 = ROcp0_25 * C6 + C4 * S6;
            ROcp0_36 = ROcp0_35 * C6 + S4 * S6;
            ROcp0_46 = -C5 * S6;
            ROcp0_56 = -(ROcp0_25 * S6 - C4 * C6);
            ROcp0_66 = -(ROcp0_35 * S6 - S4 * C6);
            ROcp0_47 = ROcp0_46 * C7 + S5 * S7;
            ROcp0_57 = ROcp0_56 * C7 + ROcp0_85 * S7;
            ROcp0_67 = ROcp0_66 * C7 + ROcp0_95 * S7;
            ROcp0_77 = -(ROcp0_46 * S7 - S5 * C7);
            ROcp0_87 = -(ROcp0_56 * S7 - ROcp0_85 * C7);
            ROcp0_97 = -(ROcp0_66 * S7 - ROcp0_95 * C7);
            ROcp0_18 = ROcp0_16 * C8 - ROcp0_77 * S8;
            ROcp0_28 = ROcp0_26 * C8 - ROcp0_87 * S8;
            ROcp0_38 = ROcp0_36 * C8 - ROcp0_97 * S8;
            ROcp0_78 = ROcp0_16 * S8 + ROcp0_77 * C8;
            ROcp0_88 = ROcp0_26 * S8 + ROcp0_87 * C8;
            ROcp0_98 = ROcp0_36 * S8 + ROcp0_97 * C8;
            ROcp0_19 = ROcp0_18 * C9 + ROcp0_47 * S9;
            ROcp0_29 = ROcp0_28 * C9 + ROcp0_57 * S9;
            ROcp0_39 = ROcp0_38 * C9 + ROcp0_67 * S9;
            ROcp0_49 = -(ROcp0_18 * S9 - ROcp0_47 * C9);
            ROcp0_59 = -(ROcp0_28 * S9 - ROcp0_57 * C9);
            ROcp0_69 = -(ROcp0_38 * S9 - ROcp0_67 * C9);
            ROcp0_410 = ROcp0_49 * C10 + ROcp0_78 * S10;
            ROcp0_510 = ROcp0_59 * C10 + ROcp0_88 * S10;
            ROcp0_610 = ROcp0_69 * C10 + ROcp0_98 * S10;
            ROcp0_710 = -(ROcp0_49 * S10 - ROcp0_78 * C10);
            ROcp0_810 = -(ROcp0_59 * S10 - ROcp0_88 * C10);
            ROcp0_910 = -(ROcp0_69 * S10 - ROcp0_98 * C10);
            ROcp0_111 = ROcp0_19 * C11 - ROcp0_710 * S11;
            ROcp0_211 = ROcp0_29 * C11 - ROcp0_810 * S11;
            ROcp0_311 = ROcp0_39 * C11 - ROcp0_910 * S11;
            ROcp0_711 = ROcp0_19 * S11 + ROcp0_710 * C11;
            ROcp0_811 = ROcp0_29 * S11 + ROcp0_810 * C11;
            ROcp0_911 = ROcp0_39 * S11 + ROcp0_910 * C11;
            ROcp0_412 = ROcp0_410 * C12 + ROcp0_711 * S12;
            ROcp0_512 = ROcp0_510 * C12 + ROcp0_811 * S12;
            ROcp0_612 = ROcp0_610 * C12 + ROcp0_911 * S12;
            ROcp0_712 = -(ROcp0_410 * S12 - ROcp0_711 * C12);
            ROcp0_812 = -(ROcp0_510 * S12 - ROcp0_811 * C12);
            ROcp0_912 = -(ROcp0_610 * S12 - ROcp0_911 * C12);
            ROcp0_113 = ROcp0_111 * C13 + ROcp0_412 * S13;
            ROcp0_213 = ROcp0_211 * C13 + ROcp0_512 * S13;
            ROcp0_313 = ROcp0_311 * C13 + ROcp0_612 * S13;
            ROcp0_413 = -(ROcp0_111 * S13 - ROcp0_412 * C13);
            ROcp0_513 = -(ROcp0_211 * S13 - ROcp0_512 * C13);
            ROcp0_613 = -(ROcp0_311 * S13 - ROcp0_612 * C13);
            ROcp0_415 = ROcp0_413 * C15 + ROcp0_712 * S15;
            ROcp0_515 = ROcp0_513 * C15 + ROcp0_812 * S15;
            ROcp0_615 = ROcp0_613 * C15 + ROcp0_912 * S15;
            ROcp0_715 = -(ROcp0_413 * S15 - ROcp0_712 * C15);
            ROcp0_815 = -(ROcp0_513 * S15 - ROcp0_812 * C15);
            ROcp0_915 = -(ROcp0_613 * S15 - ROcp0_912 * C15);
            ROcp0_116 = ROcp0_113 * C16 + ROcp0_415 * S16;
            ROcp0_216 = ROcp0_213 * C16 + ROcp0_515 * S16;
            ROcp0_316 = ROcp0_313 * C16 + ROcp0_615 * S16;
            ROcp0_416 = -(ROcp0_113 * S16 - ROcp0_415 * C16);
            ROcp0_516 = -(ROcp0_213 * S16 - ROcp0_515 * C16);
            ROcp0_616 = -(ROcp0_313 * S16 - ROcp0_615 * C16);
            OMcp0_25 = qd[5] * C4;
            OMcp0_35 = qd[5] * S4;
            OMcp0_16 = qd[4] + qd[6] * S5;
            OMcp0_26 = OMcp0_25 + ROcp0_85 * qd[6];
            OMcp0_36 = OMcp0_35 + ROcp0_95 * qd[6];
            OMcp0_17 = OMcp0_16 + ROcp0_16 * qd[7];
            OMcp0_27 = OMcp0_26 + ROcp0_26 * qd[7];
            OMcp0_37 = OMcp0_36 + ROcp0_36 * qd[7];
            OMcp0_18 = OMcp0_17 + ROcp0_47 * qd[8];
            OMcp0_28 = OMcp0_27 + ROcp0_57 * qd[8];
            OMcp0_38 = OMcp0_37 + ROcp0_67 * qd[8];
            OPcp0_18 = qdd[4] + ROcp0_16 * qdd[7] + ROcp0_47 * qdd[8] + qdd[6] * S5 + qd[5] * qd[6] * C5 + qd[7] * (OMcp0_26 * ROcp0_36 - OMcp0_36 * ROcp0_26)
                       + qd[8] * (OMcp0_27 * ROcp0_67 - OMcp0_37 * ROcp0_57);
            OPcp0_28 = ROcp0_26 * qdd[7] + ROcp0_57 * qdd[8] + ROcp0_85 * qdd[6] + qdd[5] * C4 - qd[4] * qd[5] * S4 + qd[6] * (OMcp0_35 * S5 - ROcp0_95 * qd[4])
                       - qd[7] * (OMcp0_16 * ROcp0_36 - OMcp0_36 * ROcp0_16) - qd[8] * (OMcp0_17 * ROcp0_67 - OMcp0_37 * ROcp0_47);
            OPcp0_38 = ROcp0_36 * qdd[7] + ROcp0_67 * qdd[8] + ROcp0_95 * qdd[6] + qdd[5] * S4 + qd[4] * qd[5] * C4 - qd[6] * (OMcp0_25 * S5 - ROcp0_85 * qd[4])
                       + qd[7] * (OMcp0_16 * ROcp0_26 - OMcp0_26 * ROcp0_16) + qd[8] * (OMcp0_17 * ROcp0_57 - OMcp0_27 * ROcp0_47);
            RLcp0_19 = ROcp0_78 * DPT_3_1;
            RLcp0_29 = ROcp0_88 * DPT_3_1;
            RLcp0_39 = ROcp0_98 * DPT_3_1;
            OMcp0_19 = OMcp0_18 + ROcp0_78 * qd[9];
            OMcp0_29 = OMcp0_28 + ROcp0_88 * qd[9];
            OMcp0_39 = OMcp0_38 + ROcp0_98 * qd[9];
            ORcp0_19 = OMcp0_28 * RLcp0_39 - OMcp0_38 * RLcp0_29;
            ORcp0_29 = -(OMcp0_18 * RLcp0_39 - OMcp0_38 * RLcp0_19);
            ORcp0_39 = OMcp0_18 * RLcp0_29 - OMcp0_28 * RLcp0_19;
            OPcp0_19 = OPcp0_18 + ROcp0_78 * qdd[9] + qd[9] * (OMcp0_28 * ROcp0_98 - OMcp0_38 * ROcp0_88);
            OPcp0_29 = OPcp0_28 + ROcp0_88 * qdd[9] - qd[9] * (OMcp0_18 * ROcp0_98 - OMcp0_38 * ROcp0_78);
            OPcp0_39 = OPcp0_38 + ROcp0_98 * qdd[9] + qd[9] * (OMcp0_18 * ROcp0_88 - OMcp0_28 * ROcp0_78);
            RLcp0_110 = ROcp0_78 * DPT_3_2;
            RLcp0_210 = ROcp0_88 * DPT_3_2;
            RLcp0_310 = ROcp0_98 * DPT_3_2;
            OMcp0_110 = OMcp0_19 + ROcp0_19 * qd[10];
            OMcp0_210 = OMcp0_29 + ROcp0_29 * qd[10];
            OMcp0_310 = OMcp0_39 + ROcp0_39 * qd[10];
            ORcp0_110 = OMcp0_29 * RLcp0_310 - OMcp0_39 * RLcp0_210;
            ORcp0_210 = -(OMcp0_19 * RLcp0_310 - OMcp0_39 * RLcp0_110);
            ORcp0_310 = OMcp0_19 * RLcp0_210 - OMcp0_29 * RLcp0_110;
            OPcp0_110 = OPcp0_19 + ROcp0_19 * qdd[10] + qd[10] * (OMcp0_29 * ROcp0_39 - OMcp0_39 * ROcp0_29);
            OPcp0_210 = OPcp0_29 + ROcp0_29 * qdd[10] - qd[10] * (OMcp0_19 * ROcp0_39 - OMcp0_39 * ROcp0_19);
            OPcp0_310 = OPcp0_39 + ROcp0_39 * qdd[10] + qd[10] * (OMcp0_19 * ROcp0_29 - OMcp0_29 * ROcp0_19);
            RLcp0_111 = ROcp0_410 * DPT_2_3;
            RLcp0_211 = ROcp0_510 * DPT_2_3;
            RLcp0_311 = ROcp0_610 * DPT_2_3;
            OMcp0_111 = OMcp0_110 + ROcp0_410 * qd[11];
            OMcp0_211 = OMcp0_210 + ROcp0_510 * qd[11];
            OMcp0_311 = OMcp0_310 + ROcp0_610 * qd[11];
            ORcp0_111 = OMcp0_210 * RLcp0_311 - OMcp0_310 * RLcp0_211;
            ORcp0_211 = -(OMcp0_110 * RLcp0_311 - OMcp0_310 * RLcp0_111);
            ORcp0_311 = OMcp0_110 * RLcp0_211 - OMcp0_210 * RLcp0_111;
            OPcp0_111 = OPcp0_110 + ROcp0_410 * qdd[11] + qd[11] * (OMcp0_210 * ROcp0_610 - OMcp0_310 * ROcp0_510);
            OPcp0_211 = OPcp0_210 + ROcp0_510 * qdd[11] - qd[11] * (OMcp0_110 * ROcp0_610 - OMcp0_310 * ROcp0_410);
            OPcp0_311 = OPcp0_310 + ROcp0_610 * qdd[11] + qd[11] * (OMcp0_110 * ROcp0_510 - OMcp0_210 * ROcp0_410);
            RLcp0_112 = ROcp0_111 * DPT_1_4 + ROcp0_410 * DPT_2_4 + ROcp0_711 * DPT_3_4;
            RLcp0_212 = ROcp0_211 * DPT_1_4 + ROcp0_510 * DPT_2_4 + ROcp0_811 * DPT_3_4;
            RLcp0_312 = ROcp0_311 * DPT_1_4 + ROcp0_610 * DPT_2_4 + ROcp0_911 * DPT_3_4;
            OMcp0_112 = OMcp0_111 + ROcp0_111 * qd[12];
            OMcp0_212 = OMcp0_211 + ROcp0_211 * qd[12];
            OMcp0_312 = OMcp0_311 + ROcp0_311 * qd[12];
            ORcp0_112 = OMcp0_211 * RLcp0_312 - OMcp0_311 * RLcp0_212;
            ORcp0_212 = -(OMcp0_111 * RLcp0_312 - OMcp0_311 * RLcp0_112);
            ORcp0_312 = OMcp0_111 * RLcp0_212 - OMcp0_211 * RLcp0_112;
            OMcp0_113 = OMcp0_112 + ROcp0_712 * qd[13];
            OMcp0_213 = OMcp0_212 + ROcp0_812 * qd[13];
            OMcp0_313 = OMcp0_312 + ROcp0_912 * qd[13];
            OPcp0_113 = OPcp0_111 + ROcp0_111 * qdd[12] + ROcp0_712 * qdd[13] + qd[12] * (OMcp0_211 * ROcp0_311 - OMcp0_311 * ROcp0_211) + qd[13] * (
                            OMcp0_212 * ROcp0_912 - OMcp0_312 * ROcp0_812);
            OPcp0_213 = OPcp0_211 + ROcp0_211 * qdd[12] + ROcp0_812 * qdd[13] - qd[12] * (OMcp0_111 * ROcp0_311 - OMcp0_311 * ROcp0_111) - qd[13] * (
                            OMcp0_112 * ROcp0_912 - OMcp0_312 * ROcp0_712);
            OPcp0_313 = OPcp0_311 + ROcp0_311 * qdd[12] + ROcp0_912 * qdd[13] + qd[12] * (OMcp0_111 * ROcp0_211 - OMcp0_211 * ROcp0_111) + qd[13] * (
                            OMcp0_112 * ROcp0_812 - OMcp0_212 * ROcp0_712);
            RLcp0_114 = ROcp0_413 * q[14];
            RLcp0_214 = ROcp0_513 * q[14];
            RLcp0_314 = ROcp0_613 * q[14];
            POcp0_114 = RLcp0_110 + RLcp0_111 + RLcp0_112 + RLcp0_114 + RLcp0_19 + q[1];
            POcp0_214 = RLcp0_210 + RLcp0_211 + RLcp0_212 + RLcp0_214 + RLcp0_29 + q[2];
            POcp0_314 = RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_314 + RLcp0_39 + q[3];
            JTcp0_214_4 = -(RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_314 + RLcp0_39);
            JTcp0_314_4 = RLcp0_210 + RLcp0_211 + RLcp0_212 + RLcp0_214 + RLcp0_29;
            JTcp0_114_5 = C4 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) - S4 * (RLcp0_210 + RLcp0_29) - S4 * (RLcp0_211 + RLcp0_212) - RLcp0_214 *
                          S4 + RLcp0_314 * C4;
            JTcp0_214_5 = S4 * (RLcp0_110 + RLcp0_111 + RLcp0_112 + RLcp0_114 + RLcp0_19);
            JTcp0_314_5 = -C4 * (RLcp0_110 + RLcp0_111 + RLcp0_112 + RLcp0_114 + RLcp0_19);
            JTcp0_114_6 = ROcp0_85 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) - ROcp0_95 * (RLcp0_210 + RLcp0_29) - ROcp0_95 * (RLcp0_211 +
                          RLcp0_212) - RLcp0_214 * ROcp0_95 + RLcp0_314 * ROcp0_85;
            JTcp0_214_6 = -(RLcp0_314 * S5 - ROcp0_95 * (RLcp0_110 + RLcp0_111 + RLcp0_112 + RLcp0_114 + RLcp0_19) + S5 * (RLcp0_310 + RLcp0_39) + S5 * (
                                RLcp0_311 + RLcp0_312));
            JTcp0_314_6 = RLcp0_214 * S5 - ROcp0_85 * (RLcp0_110 + RLcp0_111 + RLcp0_112 + RLcp0_114 + RLcp0_19) + S5 * (RLcp0_210 + RLcp0_29) + S5 * (
                              RLcp0_211 + RLcp0_212);
            JTcp0_114_7 = ROcp0_26 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) - ROcp0_36 * (RLcp0_210 + RLcp0_29) - ROcp0_36 * (RLcp0_211 +
                          RLcp0_212) - RLcp0_214 * ROcp0_36 + RLcp0_314 * ROcp0_26;
            JTcp0_214_7 = RLcp0_114 * ROcp0_36 - RLcp0_314 * ROcp0_16 - ROcp0_16 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) + ROcp0_36 * (
                              RLcp0_110 + RLcp0_19) + ROcp0_36 * (RLcp0_111 + RLcp0_112);
            JTcp0_314_7 = ROcp0_16 * (RLcp0_210 + RLcp0_211 + RLcp0_212 + RLcp0_29) - ROcp0_26 * (RLcp0_110 + RLcp0_19) - ROcp0_26 * (RLcp0_111 +
                          RLcp0_112) - RLcp0_114 * ROcp0_26 + RLcp0_214 * ROcp0_16;
            JTcp0_114_8 = ROcp0_57 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) - ROcp0_67 * (RLcp0_210 + RLcp0_29) - ROcp0_67 * (RLcp0_211 +
                          RLcp0_212) - RLcp0_214 * ROcp0_67 + RLcp0_314 * ROcp0_57;
            JTcp0_214_8 = RLcp0_114 * ROcp0_67 - RLcp0_314 * ROcp0_47 - ROcp0_47 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_39) + ROcp0_67 * (
                              RLcp0_110 + RLcp0_19) + ROcp0_67 * (RLcp0_111 + RLcp0_112);
            JTcp0_314_8 = ROcp0_47 * (RLcp0_210 + RLcp0_211 + RLcp0_212 + RLcp0_29) - ROcp0_57 * (RLcp0_110 + RLcp0_19) - ROcp0_57 * (RLcp0_111 +
                          RLcp0_112) - RLcp0_114 * ROcp0_57 + RLcp0_214 * ROcp0_47;
            JTcp0_114_9 = ROcp0_88 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_314) - ROcp0_98 * (RLcp0_210 + RLcp0_211) - ROcp0_98 * (RLcp0_212 +
                          RLcp0_214);
            JTcp0_214_9 = -(ROcp0_78 * (RLcp0_310 + RLcp0_311 + RLcp0_312 + RLcp0_314) - ROcp0_98 * (RLcp0_110 + RLcp0_111) - ROcp0_98 * (RLcp0_112 +
                            RLcp0_114));
            JTcp0_314_9 = ROcp0_78 * (RLcp0_210 + RLcp0_211 + RLcp0_212 + RLcp0_214) - ROcp0_88 * (RLcp0_110 + RLcp0_111) - ROcp0_88 * (RLcp0_112 +
                          RLcp0_114);
            JTcp0_114_10 = ROcp0_29 * (RLcp0_311 + RLcp0_312) - ROcp0_39 * (RLcp0_211 + RLcp0_212) - RLcp0_214 * ROcp0_39 + RLcp0_314 * ROcp0_29;
            JTcp0_214_10 = RLcp0_114 * ROcp0_39 - RLcp0_314 * ROcp0_19 - ROcp0_19 * (RLcp0_311 + RLcp0_312) + ROcp0_39 * (RLcp0_111 + RLcp0_112);
            JTcp0_314_10 = ROcp0_19 * (RLcp0_211 + RLcp0_212) - ROcp0_29 * (RLcp0_111 + RLcp0_112) - RLcp0_114 * ROcp0_29 + RLcp0_214 * ROcp0_19;
            JTcp0_114_11 = ROcp0_510 * (RLcp0_312 + RLcp0_314) - ROcp0_610 * (RLcp0_212 + RLcp0_214);
            JTcp0_214_11 = -(ROcp0_410 * (RLcp0_312 + RLcp0_314) - ROcp0_610 * (RLcp0_112 + RLcp0_114));
            JTcp0_314_11 = ROcp0_410 * (RLcp0_212 + RLcp0_214) - ROcp0_510 * (RLcp0_112 + RLcp0_114);
            JTcp0_114_12 = -(RLcp0_214 * ROcp0_311 - RLcp0_314 * ROcp0_211);
            JTcp0_214_12 = RLcp0_114 * ROcp0_311 - RLcp0_314 * ROcp0_111;
            JTcp0_314_12 = -(RLcp0_114 * ROcp0_211 - RLcp0_214 * ROcp0_111);
            JTcp0_114_13 = -(RLcp0_214 * ROcp0_912 - RLcp0_314 * ROcp0_812);
            JTcp0_214_13 = RLcp0_114 * ROcp0_912 - RLcp0_314 * ROcp0_712;
            JTcp0_314_13 = -(RLcp0_114 * ROcp0_812 - RLcp0_214 * ROcp0_712);
            ORcp0_114 = OMcp0_213 * RLcp0_314 - OMcp0_313 * RLcp0_214;
            ORcp0_214 = -(OMcp0_113 * RLcp0_314 - OMcp0_313 * RLcp0_114);
            ORcp0_314 = OMcp0_113 * RLcp0_214 - OMcp0_213 * RLcp0_114;
            VIcp0_114 = ORcp0_110 + ORcp0_111 + ORcp0_112 + ORcp0_114 + ORcp0_19 + qd[1] + ROcp0_413 * qd[14];
            VIcp0_214 = ORcp0_210 + ORcp0_211 + ORcp0_212 + ORcp0_214 + ORcp0_29 + qd[2] + ROcp0_513 * qd[14];
            VIcp0_314 = ORcp0_310 + ORcp0_311 + ORcp0_312 + ORcp0_314 + ORcp0_39 + qd[3] + ROcp0_613 * qd[14];
            ACcp0_114 = qdd[1] + OMcp0_210 * ORcp0_311 + OMcp0_211 * ORcp0_312 + OMcp0_213 * ORcp0_314 + OMcp0_28 * ORcp0_39 + OMcp0_29 * ORcp0_310 -
                        OMcp0_310 * ORcp0_211 - OMcp0_311 * ORcp0_212 - OMcp0_313 * ORcp0_214 - OMcp0_38 * ORcp0_29 - OMcp0_39 * ORcp0_210 + OPcp0_210 * RLcp0_311 +
                        OPcp0_211 * RLcp0_312 + OPcp0_213 * RLcp0_314 + OPcp0_28 * RLcp0_39 + OPcp0_29 * RLcp0_310 - OPcp0_310 * RLcp0_211 - OPcp0_311 * RLcp0_212 -
                        OPcp0_313 * RLcp0_214 - OPcp0_38 * RLcp0_29 - OPcp0_39 * RLcp0_210 + ROcp0_413 * qdd[14] + (2.0) * qd[14] * (OMcp0_213 * ROcp0_613 - OMcp0_313 * ROcp0_513);
            ACcp0_214 = qdd[2] - OMcp0_110 * ORcp0_311 - OMcp0_111 * ORcp0_312 - OMcp0_113 * ORcp0_314 - OMcp0_18 * ORcp0_39 - OMcp0_19 * ORcp0_310 +
                        OMcp0_310 * ORcp0_111 + OMcp0_311 * ORcp0_112 + OMcp0_313 * ORcp0_114 + OMcp0_38 * ORcp0_19 + OMcp0_39 * ORcp0_110 - OPcp0_110 * RLcp0_311 -
                        OPcp0_111 * RLcp0_312 - OPcp0_113 * RLcp0_314 - OPcp0_18 * RLcp0_39 - OPcp0_19 * RLcp0_310 + OPcp0_310 * RLcp0_111 + OPcp0_311 * RLcp0_112 +
                        OPcp0_313 * RLcp0_114 + OPcp0_38 * RLcp0_19 + OPcp0_39 * RLcp0_110 + ROcp0_513 * qdd[14] - (2.0) * qd[14] * (OMcp0_113 * ROcp0_613 - OMcp0_313 * ROcp0_413);
            ACcp0_314 = qdd[3] + OMcp0_110 * ORcp0_211 + OMcp0_111 * ORcp0_212 + OMcp0_113 * ORcp0_214 + OMcp0_18 * ORcp0_29 + OMcp0_19 * ORcp0_210 -
                        OMcp0_210 * ORcp0_111 - OMcp0_211 * ORcp0_112 - OMcp0_213 * ORcp0_114 - OMcp0_28 * ORcp0_19 - OMcp0_29 * ORcp0_110 + OPcp0_110 * RLcp0_211 +
                        OPcp0_111 * RLcp0_212 + OPcp0_113 * RLcp0_214 + OPcp0_18 * RLcp0_29 + OPcp0_19 * RLcp0_210 - OPcp0_210 * RLcp0_111 - OPcp0_211 * RLcp0_112 -
                        OPcp0_213 * RLcp0_114 - OPcp0_28 * RLcp0_19 - OPcp0_29 * RLcp0_110 + ROcp0_613 * qdd[14] + (2.0) * qd[14] * (OMcp0_113 * ROcp0_513 - OMcp0_213 * ROcp0_413);
            OMcp0_115 = OMcp0_113 + ROcp0_113 * qd[15];
            OMcp0_215 = OMcp0_213 + ROcp0_213 * qd[15];
            OMcp0_315 = OMcp0_313 + ROcp0_313 * qd[15];
            OMcp0_116 = OMcp0_115 + ROcp0_715 * qd[16];
            OMcp0_216 = OMcp0_215 + ROcp0_815 * qd[16];
            OMcp0_316 = OMcp0_315 + ROcp0_915 * qd[16];
            OPcp0_116 = OPcp0_113 + ROcp0_113 * qdd[15] + ROcp0_715 * qdd[16] + qd[15] * (OMcp0_213 * ROcp0_313 - OMcp0_313 * ROcp0_213) + qd[16] * (
                            OMcp0_215 * ROcp0_915 - OMcp0_315 * ROcp0_815);
            OPcp0_216 = OPcp0_213 + ROcp0_213 * qdd[15] + ROcp0_815 * qdd[16] - qd[15] * (OMcp0_113 * ROcp0_313 - OMcp0_313 * ROcp0_113) - qd[16] * (
                            OMcp0_115 * ROcp0_915 - OMcp0_315 * ROcp0_715);
            OPcp0_316 = OPcp0_313 + ROcp0_313 * qdd[15] + ROcp0_915 * qdd[16] + qd[15] * (OMcp0_113 * ROcp0_213 - OMcp0_213 * ROcp0_113) + qd[16] * (
                            OMcp0_115 * ROcp0_815 - OMcp0_215 * ROcp0_715);

            // = = Block_1_0_0_1_1_0 = =

            // Symbolic Outputs

            sens->P[1] = POcp0_114;
            sens->P[2] = POcp0_214;
            sens->P[3] = POcp0_314;
            sens->R[1][1] = ROcp0_116;
            sens->R[1][2] = ROcp0_216;
            sens->R[1][3] = ROcp0_316;
            sens->R[2][1] = ROcp0_416;
            sens->R[2][2] = ROcp0_516;
            sens->R[2][3] = ROcp0_616;
            sens->R[3][1] = ROcp0_715;
            sens->R[3][2] = ROcp0_815;
            sens->R[3][3] = ROcp0_915;
            sens->V[1] = VIcp0_114;
            sens->V[2] = VIcp0_214;
            sens->V[3] = VIcp0_314;
            sens->OM[1] = OMcp0_116;
            sens->OM[2] = OMcp0_216;
            sens->OM[3] = OMcp0_316;
            sens->J[1][1] = (1.0);
            sens->J[1][5] = JTcp0_114_5;
            sens->J[1][6] = JTcp0_114_6;
            sens->J[1][7] = JTcp0_114_7;
            sens->J[1][8] = JTcp0_114_8;
            sens->J[1][9] = JTcp0_114_9;
            sens->J[1][10] = JTcp0_114_10;
            sens->J[1][11] = JTcp0_114_11;
            sens->J[1][12] = JTcp0_114_12;
            sens->J[1][13] = JTcp0_114_13;
            sens->J[1][14] = ROcp0_413;
            sens->J[2][2] = (1.0);
            sens->J[2][4] = JTcp0_214_4;
            sens->J[2][5] = JTcp0_214_5;
            sens->J[2][6] = JTcp0_214_6;
            sens->J[2][7] = JTcp0_214_7;
            sens->J[2][8] = JTcp0_214_8;
            sens->J[2][9] = JTcp0_214_9;
            sens->J[2][10] = JTcp0_214_10;
            sens->J[2][11] = JTcp0_214_11;
            sens->J[2][12] = JTcp0_214_12;
            sens->J[2][13] = JTcp0_214_13;
            sens->J[2][14] = ROcp0_513;
            sens->J[3][3] = (1.0);
            sens->J[3][4] = JTcp0_314_4;
            sens->J[3][5] = JTcp0_314_5;
            sens->J[3][6] = JTcp0_314_6;
            sens->J[3][7] = JTcp0_314_7;
            sens->J[3][8] = JTcp0_314_8;
            sens->J[3][9] = JTcp0_314_9;
            sens->J[3][10] = JTcp0_314_10;
            sens->J[3][11] = JTcp0_314_11;
            sens->J[3][12] = JTcp0_314_12;
            sens->J[3][13] = JTcp0_314_13;
            sens->J[3][14] = ROcp0_613;
            sens->J[4][4] = (1.0);
            sens->J[4][6] = S5;
            sens->J[4][7] = ROcp0_16;
            sens->J[4][8] = ROcp0_47;
            sens->J[4][9] = ROcp0_78;
            sens->J[4][10] = ROcp0_19;
            sens->J[4][11] = ROcp0_410;
            sens->J[4][12] = ROcp0_111;
            sens->J[4][13] = ROcp0_712;
            sens->J[4][15] = ROcp0_113;
            sens->J[4][16] = ROcp0_715;
            sens->J[5][5] = C4;
            sens->J[5][6] = ROcp0_85;
            sens->J[5][7] = ROcp0_26;
            sens->J[5][8] = ROcp0_57;
            sens->J[5][9] = ROcp0_88;
            sens->J[5][10] = ROcp0_29;
            sens->J[5][11] = ROcp0_510;
            sens->J[5][12] = ROcp0_211;
            sens->J[5][13] = ROcp0_812;
            sens->J[5][15] = ROcp0_213;
            sens->J[5][16] = ROcp0_815;
            sens->J[6][5] = S4;
            sens->J[6][6] = ROcp0_95;
            sens->J[6][7] = ROcp0_36;
            sens->J[6][8] = ROcp0_67;
            sens->J[6][9] = ROcp0_98;
            sens->J[6][10] = ROcp0_39;
            sens->J[6][11] = ROcp0_610;
            sens->J[6][12] = ROcp0_311;
            sens->J[6][13] = ROcp0_912;
            sens->J[6][15] = ROcp0_313;
            sens->J[6][16] = ROcp0_915;
            sens->A[1] = ACcp0_114;
            sens->A[2] = ACcp0_214;
            sens->A[3] = ACcp0_314;
            sens->OMP[1] = OPcp0_116;
            sens->OMP[2] = OPcp0_216;
            sens->OMP[3] = OPcp0_316;

            break;
        default:
            break;
    }


    // ====== END Task 1 ======


}

