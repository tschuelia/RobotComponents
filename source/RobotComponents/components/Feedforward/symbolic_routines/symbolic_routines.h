
#ifndef _SYMP_ROUTINES_HH_
#define _SYMP_ROUTINES_HH_

#ifdef __cplusplus
extern "C" {
#endif

void  mbs_sensor_ArmarIV_GazeStab_virt(MbsSensor* sens,
                                       double* q,
                                       double* qd,
                                       double* qdd,
                                       int isens);

void mbs_sensor_ArmarIV_W_Torso_virt(MbsSensor* sens,
                                     double* q,
                                     double* qd,
                                     double* qdd,
                                     int isens);

void  mbs_sensor_ArmarIII_simplified_virt(MbsSensor* sens,
        double* q_dof,
        double* qd_dof,
        int isens);

#ifdef __cplusplus
}
#endif

#endif
