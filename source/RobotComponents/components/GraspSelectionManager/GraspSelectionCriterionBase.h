/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ROBOTCOMPONENTS_GraspSelectionCriterionBase_H
#define _ROBOTCOMPONENTS_GraspSelectionCriterionBase_H

#include <RobotComponents/interface/components/GraspingManager/GraspSelectionManagerInterface.h>

#include <ArmarXCore/core/Component.h>

namespace armarx {

    class GraspSelectionCriterionPropertyDefinitions:
            public ComponentPropertyDefinitions
    {
    public:
        GraspSelectionCriterionPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("GraspSelectionManagerName", "GraspSelectionManager", "Name of the GraspSelectionManager proxy");
        }
    };

    class GraspSelectionCriterionBase :
            virtual public GraspSelectionCriterionInterface,
            virtual public Component
    {
    public:
        GraspSelectionCriterionBase();
        virtual void onInitGraspSelectionCriterion() = 0;
        virtual void onConnectGraspSelectionCriterion() = 0;

    protected:
        GraspSelectionManagerInterfacePrx graspSelectionManager;

        // ManagedIceObject interface
    protected:
        void onInitComponent();
        void onConnectComponent();

        virtual PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return PropertyDefinitionsPtr(
                        new GraspSelectionCriterionPropertyDefinitions(
                                getConfigIdentifier()));
        }

    };

}

#endif
