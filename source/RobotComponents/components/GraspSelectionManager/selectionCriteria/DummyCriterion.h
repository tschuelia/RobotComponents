/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck ( valerij.wittenbeck at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ROBOTCOMPONENTS_DummyCriterion_H
#define ROBOTCOMPONENTS_DummyCriterion_H

#include <RobotComponents/components/GraspSelectionManager/GraspSelectionCriterionBase.h>
#include <MemoryX/components/CommonPlacesLearner/CommonPlacesLearner.h>
#include <ArmarXCore/core/Component.h>

namespace armarx
{
    class DummyCriterionPropertyDefinitions:
        public GraspSelectionCriterionPropertyDefinitions
    {
    public:
        DummyCriterionPropertyDefinitions(std::string prefix):
            GraspSelectionCriterionPropertyDefinitions(prefix)
        {
            //            defineOptionalProperty<std::string>("CommonPlacesLearnerName", "CommonPlacesLearnerGraspSelectionCriterion", "The CommonPlacesLearner to use");
        }
    };

    class DummyCriterion : public GraspSelectionCriterionBase
    {
    public:
        DummyCriterion();

        // ManagedIceObject interface
    protected:
        void onInitGraspSelectionCriterion();
        void onConnectGraspSelectionCriterion();
        std::string getDefaultName() const;

        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return armarx::PropertyDefinitionsPtr(
                       new DummyCriterionPropertyDefinitions(
                           getConfigIdentifier()));
        }

    public:
        GeneratedGraspList filterGrasps(const GeneratedGraspList& grasps, const Ice::Current&);

    };

} // namespace spoac

#endif // SPOAC_DummyCriterion_H
