/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::GraspingManager
 * @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GraspingManager.h"
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
#include <IceUtil/UUID.h>
#include <tuple>
#include <atomic>

#include <VirtualRobot/RobotConfig.h>

using namespace armarx;

static const DrawColor COLOR_POSE_LINE
{
    0.5f, 1.0f, 0.5f, 0.5f
};
static const DrawColor COLOR_POSE_POINT
{
    0.0f, 1.0f, 0.0f, 0.5f
};
static const DrawColor COLOR_CONFIG_LINE
{
    1.0f, 0.5f, 0.5f, 0.5f
};
static const DrawColor COLOR_CONFIG_POINT
{
    1.0f, 0.0f, 0.0f, 0.5f
};
static const DrawColor COLOR_ROBOT
{
    0.0f, 0.586f, 0.508f, 0.7f
};

static const float LINE_WIDTH = 5.f;
static const float SPHERE_SIZE = 6.f;

static const std::map<std::string, std::string> TCP_HAND_MAPPING
{
    {"TCP R", "handright3a"},
    {"TCP L", "handleft3a"},
    {"Hand L TCP", "handleftarmar6"},
    {"Hand R TCP", "handrightarmar6"}
};



auto newId = []() mutable
{
    static std::atomic<int> i {0};
    return std::to_string(i++);
};

void GraspingManager::onInitComponent()
{
    graspGeneratorName = getProperty<std::string>("GraspGeneratorName").getValue();
    robotPlacementName = getProperty<std::string>("RobotPlacementName").getValue();
    robotNodeSetNames = armarx::Split(getProperty<std::string>("RobotNodeSetNames").getValue(), ";");
    reachabilitySpaceFilePaths = armarx::Split(getProperty<std::string>("ReachabilitySpaceFilePaths").getValue(), ";");

    //@TODO still not sure if this is the way to go
    for (auto& path : reachabilitySpaceFilePaths)
    {
        std::string packageName = boost::filesystem::path {path} .begin()->string();
        ARMARX_CHECK_EXPRESSION_W_HINT(!packageName.empty(), "Path '" << path << "' could not be parsed correctly");
        armarx::CMakePackageFinder project(packageName);
        path = project.getDataDir() + "/" + path;
    }

    offeringTopic("DebugDrawerUpdates");

    usingProxy(graspGeneratorName);
    usingProxy("GraspSelectionManager");
    usingProxy(robotPlacementName);

    usingProxy("PlannedMotionProvider");

    usingProxy("RobotIK");
    usingProxy("RobotStateComponent");

    usingProxy("CommonStorage");
    usingProxy("WorkingMemory");
    usingProxy("PriorKnowledge");
}

void GraspingManager::onConnectComponent()
{
    entityDrawer = getTopic<memoryx::EntityDrawerInterfacePrx>("DebugDrawerUpdates");
    layerName = getDefaultName();
    assignProxy(gg, graspGeneratorName);
    assignProxy(gsm, "GraspSelectionManager");
    assignProxy(rp, robotPlacementName);

    assignProxy(pmp, "PlannedMotionProvider");

    assignProxy(rik, "RobotIK");
    assignProxy(rsc, "RobotStateComponent");
    localRobot = armarx::RemoteRobot::createLocalCloneFromFile(rsc, VirtualRobot::RobotIO::eCollisionModel);
    localRobot->print();

    assignProxy(cs, "CommonStorage");
    assignProxy(wm, "WorkingMemory");
    assignProxy(prior, "PriorKnowledge");

    Ice::FloatSeq boundsStrings = Split<float>(getProperty<std::string>("PlanningBoundingBox").getValue(), ",");
    ARMARX_INFO << VAROUT(boundsStrings);
    planningBoundingBox.min.e0 = boundsStrings.at(0);
    planningBoundingBox.min.e1 = boundsStrings.at(1);
    planningBoundingBox.min.e2 = boundsStrings.at(2);
    planningBoundingBox.max.e0 = boundsStrings.at(3);
    planningBoundingBox.max.e1 = boundsStrings.at(4);
    planningBoundingBox.max.e2 = boundsStrings.at(5);

    //    bool hasUnloadedRNS = false;
    //    for (const auto& rnsName : robotNodeSetNames)
    //    {
    //        if (!rik->hasReachabilitySpace(rnsName))
    //        {
    //            ARMARX_INFO << "RNS '" << rnsName << "' has no reachability space defined.";
    //            hasUnloadedRNS = true;
    //            break;
    //        }
    //    }

    //    if (hasUnloadedRNS)
    //    {
    //        ARMARX_INFO << "at least one RNS has no reachability space defined; loading reachability files...";
    //        for (const auto& path : reachabilitySpaceFilePaths)
    //        {
    //            ARMARX_INFO << "trying to load from path '" << path << "'";
    //            bool rsLoaded = rik->loadReachabilitySpace(path);
    //            ARMARX_CHECK_EXPRESSION_W_HINT(rsLoaded, "Could not load reachability space from path '" << path << "'");
    //            ARMARX_INFO << "reachability file successfully loaded";
    //        }
    //    }

    //    for (const auto& rnsName : robotNodeSetNames)
    //    {
    //        ARMARX_CHECK_EXPRESSION_W_HINT(rik->hasReachabilitySpace(rnsName),
    //                                       "RNS '" << rnsName << "' still has no reachability space defined");
    //    }
}

void GraspingManager::onDisconnectComponent()
{

}

void GraspingManager::onExitComponent()
{

}

GeneratedGraspList GraspingManager::generateGrasps(const std::string& objectInstanceEntityId)
{

    float visuSlowdownFactor = getProperty<float>("VisualizationSlowdownFactor");


    ARMARX_VERBOSE << "Step: generate grasps";
    GeneratedGraspList grasps = gg->generateGrasps(objectInstanceEntityId);
    std::sort(grasps.begin(), grasps.end(), [](const GeneratedGrasp & l, const GeneratedGrasp & r)
    {
        return l.score < r.score;
    });
    if (grasps.empty())
    {
        ARMARX_WARNING << " Step 'generate grasps' generated no grasps";
    }
    //    auto obj = wm->getObjectInstancesSegment()->getObjectInstanceById(objectInstanceEntityId);
    auto classes = prior->getObjectClassesSegment();
    int i = 0;
    for (GeneratedGrasp& grasp : grasps)
    {
        visualizeGrasp(grasp, i);
        i++;
    }
    usleep(2000000 * visuSlowdownFactor);

    entityDrawer->removeLayer("GeneratedGrasps");
    return grasps;
}

std::pair<std::string, std::string> GraspingManager::visualizeGrasp(const GeneratedGrasp& grasp, int id, const DrawColor& color)
{
    float visuSlowdownFactor =  getProperty<float>("VisualizationSlowdownFactor").getValue();
    std::pair<std::string, std::string> result;
    if (visuSlowdownFactor  <= 0)
    {
        return result;
    }
    auto tcpName = localRobot->getEndEffector(grasp.eefName)->getTcp()->getName();
    if (!TCP_HAND_MAPPING.count(tcpName))
    {
        return result;
    }
    auto handName = TCP_HAND_MAPPING.at(tcpName);
    auto objClass = prior->getObjectClassesSegment()->getObjectClassByName(handName);
    if (objClass)
    {
        usleep(500000 * visuSlowdownFactor);
        //        entityDrawer->setPoseVisu("GeneratedGrasps", "GraspCandidate" + handName + std::to_string(id), grasp.framedPose);
        result.first = "GeneratedGrasps";
        result.second = "GraspCandidate" + handName + std::to_string(id);
        entityDrawer->setObjectVisu("GeneratedGrasps", result.second, objClass, grasp.framedPose);
        entityDrawer->updateObjectColor("GeneratedGrasps", result.second, color);
    }
    else
    {
        ARMARX_INFO << "Could not find hand with name " << handName << " in priorknowledge";
    }
    return result;
}

GeneratedGraspList GraspingManager::filterGrasps(const GeneratedGraspList& grasps)
{
    ARMARX_VERBOSE << "Step: filter grasps";
    GeneratedGraspList filteredGrasps = gsm->filterGrasps(grasps);
    if (filteredGrasps.empty())
    {
        ARMARX_WARNING << " Step 'filter generated grasps' filtered out all grasps";
    }

    return filteredGrasps;
}

GraspingPlacementList GraspingManager::generateRobotPlacements(const GeneratedGraspList& grasps, const std::string& objectInstanceEntityId)
{
    ARMARX_VERBOSE << "Step: generate robot placements";
    GraspingPlacementList graspPlacements = rp->generateRobotPlacements(grasps, objectInstanceEntityId);
    ARMARX_CHECK_EXPRESSION_W_HINT(!graspPlacements.empty(), "No placements for the robot platform were found.");
    return graspPlacements;
}

GraspingTrajectory GraspingManager::planMotion(const MotionPlanningData& mpd)
{
    entityDrawer->setPoseVisu(layerName, "MotionPlanningPlatformTargetPose", mpd.globalPoseGoal);

    ARMARX_VERBOSE << "Step: Motion Planning";
    SimoxCSpacePtr cspace = new SimoxCSpace(cs, false);

    auto robotFileName = rsc->getRobotFilename();
    auto packageName = boost::filesystem::path {robotFileName} .begin()->string();
    auto axPackages = rsc->getArmarXPackages();
    ARMARX_CHECK_EXPRESSION_W_HINT(std::find(axPackages.cbegin(), axPackages.cend(), packageName) != axPackages.cend(), "Could not determine package name from path '" << robotFileName << "', "
                                   << "because the determined package name '" << packageName << "' is not in the following list: " << axPackages);

    AgentPlanningInformation agentData;
    agentData.agentPose = mpd.globalPoseGoal;
    agentData.agentProjectNames = rsc->getArmarXPackages();
    agentData.agentRelativeFilePath = robotFileName;
    //    agentData.kinemaicChainNames = robotNodeSetNames;
    agentData.kinemaicChainNames = {mpd.rnsToUse};
    agentData.collisionSetNames = {getProperty<std::string>("RobotCollisionNodeSet").getValue()};
    agentData.initialJointValues = mpd.configStart;
    cspace->setAgent(agentData);
    cspace->addObjectsFromWorkingMemory(wm);

    entityDrawer->setPoseVisu("Poses" , "StartPoseAgent", mpd.globalPoseStart);

    SimoxCSpaceWith2DPosePtr cspacePlatform = new SimoxCSpaceWith2DPose(cs, false);
    agentData.kinemaicChainNames = {};
    agentData.collisionSetNames = {getProperty<std::string>("RobotCollisionNodeSet").getValue()};//"PlatformTorsoHeadColModel")};//InflatedPlatformTorsoHeadColModelWithFingerTips, InflatedFullCollisionModel
    cspacePlatform->setAgent(agentData);
    cspacePlatform->addObjectsFromWorkingMemory(wm);
    cspacePlatform->setPoseBounds(planningBoundingBox);

    ARMARX_VERBOSE << "CSpace created";

    return pmp->planMotion(cspace, cspacePlatform, mpd);
}



void GraspingManager::drawTrajectory(const GraspingTrajectory& t)
{
    ARMARX_VERBOSE << "Step: Draw trajectory";
    float visuSlowdownFactor = getProperty<float>("VisualizationSlowdownFactor");

    TrajectoryPtr poseTrajectory = TrajectoryPtr::dynamicCast(t.poseTrajectory);
    TrajectoryPtr configTrajectory = TrajectoryPtr::dynamicCast(t.configTrajectory);
    ARMARX_CHECK_EXPRESSION(poseTrajectory->size() != 0);
    ARMARX_CHECK_EXPRESSION(configTrajectory->size() != 0);


    auto robotId = newId();

    localRobot->setJointValues(rsc->getSynchronizedRobot()->getConfig());
    entityDrawer->setRobotVisu(layerName, robotId, rsc->getRobotFilename(), "RobotAPI", CollisionModel);
    entityDrawer->updateRobotColor(layerName, robotId, COLOR_ROBOT);
    entityDrawer->updateRobotPose(layerName, robotId, new Pose(localRobot->getGlobalPose()));
    entityDrawer->updateRobotConfig(layerName, robotId, localRobot->getConfig()->getRobotNodeJointValueMap());

    ARMARX_INFO << VAROUT(poseTrajectory->output());

    std::vector<PosePtr> poseData;
    ARMARX_CHECK_EXPRESSION_W_HINT(poseTrajectory->dim() >= 3, "dim: " << poseTrajectory->dim());
    std::transform(poseTrajectory->begin(), poseTrajectory->end(), std::back_inserter(poseData), [](const Trajectory::TrajData & data) -> PosePtr
    {
        Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
        VirtualRobot::MathTools::rpy2eigen4f(0, 0, data.getPosition(2), pose);
        pose(0, 3) = data.getPosition(0);
        pose(1, 3) = data.getPosition(1);
        pose(2, 3) = 1;
        return new Pose(pose);
    });
    int stepSize = std::max<int>(1, poseData.size() / 20);
    for (auto it = poseData.cbegin(); it != poseData.cend(); it += stepSize)
    {
        Vector3Ptr currPos = new Vector3((*it)->toEigen());
        auto nextIt = std::next(it);
        entityDrawer->setSphereVisu(layerName, newId(), currPos, COLOR_POSE_POINT, SPHERE_SIZE);
        if (nextIt != poseData.cend())
        {
            Vector3Ptr nextPos = new Vector3((*nextIt)->toEigen());
            entityDrawer->setLineVisu(layerName, newId(), currPos, nextPos, LINE_WIDTH, COLOR_POSE_LINE);
            Eigen::Matrix4f pose = localRobot->getGlobalPose();
            pose.block<3, 1>(0, 3) = nextPos->toEigen();
            entityDrawer->updateRobotPose(layerName, robotId, *nextIt);
            usleep(300000 * visuSlowdownFactor);
        }
    }

    auto targetPose = poseData.back()->toEigen();
    localRobot->setGlobalPose(targetPose);

    std::vector<NameValueMap> configData;
    ARMARX_CHECK_EXPRESSION(configTrajectory->dim() > 0);
    auto jointLabels = configTrajectory->getDimensionNames();
    std::transform(configTrajectory->begin(), configTrajectory->end(), std::back_inserter(configData), [&](const Trajectory::TrajData & data) -> NameValueMap
    {
        NameValueMap result;
        for (size_t i = 0; i < jointLabels.size(); ++i)
        {
            result.insert({jointLabels[i], data.getPosition(i)});
        }
        return result;
    });



    VirtualRobot::RobotNodeSetPtr rns = localRobot->getRobotNodeSet(t.rnsToUse);
    ARMARX_CHECK_EXPRESSION(rns && rns->getTCP());
    const auto tcpName = rns->getTCP()->getName();

    std::vector<Vector3Ptr> tcpPoseList;
    std::transform(configData.cbegin(), configData.cend(), std::back_inserter(tcpPoseList), [&](const NameValueMap & config)
    {
        localRobot->setJointValues(config);
        return new Vector3(localRobot->getRobotNode(tcpName)->getGlobalPose());
    });

    stepSize = std::max<int>(1, tcpPoseList.size() / 20);
    int i = 0;
    for (auto it = tcpPoseList.cbegin(); it != tcpPoseList.cend(); it += stepSize, i++)
    {
        auto nextIt = std::next(it);
        auto currPose = *it;
        entityDrawer->setSphereVisu(layerName, newId(), currPose, COLOR_CONFIG_POINT, SPHERE_SIZE);

        if (nextIt != tcpPoseList.cend())
        {
            auto nextPose = *nextIt;
            entityDrawer->updateRobotConfig(layerName, robotId, configData.at(i));
            entityDrawer->setLineVisu(layerName, newId(), currPose, nextPose, SPHERE_SIZE, COLOR_CONFIG_LINE);
            usleep(300000 * visuSlowdownFactor);
        }
    }

    entityDrawer->updateRobotConfig(layerName, robotId, configData.back());

    ARMARX_CHECK_EXPRESSION_W_HINT(TCP_HAND_MAPPING.find(tcpName) != TCP_HAND_MAPPING.end(), "Unknown TCP '" << tcpName << "'");
    auto handObjectClass = prior->getObjectClassesSegment()->getObjectClassByName(TCP_HAND_MAPPING.at(tcpName));
    ARMARX_CHECK_EXPRESSION_W_HINT(handObjectClass, TCP_HAND_MAPPING.at(tcpName));

    localRobot->setJointValues(configData.back());
    entityDrawer->setObjectVisu(layerName, handObjectClass->getName(), handObjectClass, new Pose(localRobot->getRobotNode(tcpName)->getGlobalPose()));
    entityDrawer->updateObjectColor(layerName, handObjectClass->getName(), COLOR_ROBOT);
}

void GraspingManager::setNextStepDescription(const std::string& description,  const std::string& objId)
{
    step++;
    auto objInstance = wm->getObjectInstancesSegment()->getObjectInstanceById(objId);
    FramedPositionPtr position = armarx::FramedPositionPtr::dynamicCast(objInstance->getPositionBase());
    position->changeToGlobal(localRobot);
    position->z += 400;
    entityDrawer->setTextVisu(layerName, "stepDescription", "Step " +  std::to_string(step) + ": " + description, position, DrawColor {0, 1, 0, 1}, 15);
}

void GraspingManager::resetStepDescription()
{
    step = 0;
    stepDescription.clear();
    entityDrawer->removeTextVisu(layerName, "stepDescription");
}

std::vector<MotionPlanningData> GraspingManager::calculateIK(const GraspingPlacementList& graspPlacements)
{
    std::vector<MotionPlanningData>mpdList;
    //    VirtualRobot::RobotPtr localRobot = RemoteRobot::createLocalCloneFromFile(rsc, VirtualRobot::RobotIO::eStructure);

    Eigen::Vector3f robotPos = localRobot->getGlobalPose().block<3, 1>(0, 3);

    ARMARX_IMPORTANT << "Robot position: " << VAROUT(robotPos);

    //    auto disableGraspVisu = [&](int id)
    //    {
    //        entityDrawer->updateObjectColor("GeneratedGrasps", "GraspCandidate" + std::to_string(id), DrawColor {1.0, 0.0, 0.0, 0.5});
    //    };
    robotVisuId = ""; // if empty, robot visu will be created
    int i = 0;
    for (const GraspingPlacement& gp : graspPlacements)
    {
        NameValueMap currentConfig = rsc->getSynchronizedRobot()->getConfig();
        ARMARX_CHECK_EXPRESSION(!currentConfig.empty());
        auto graspVisuId = visualizeGrasp(gp.grasp, i);
        Eigen::Matrix4f currentRobotPose = localRobot->getGlobalPose();
        auto desiredRobotPose = PosePtr::dynamicCast(FramedPosePtr::dynamicCast(gp.robotPose)->toGlobal(localRobot));
        auto desiredRobotPoseEigen = desiredRobotPose->toEigen();
        // objectPose is actually the tcp pose...
        //        FramedPosePtr desiredTCPPose = FramedPosePtr::dynamicCast(gp.grasp.framedPose);
        // TODO: move along gcp z axis
        //        desiredTCPPose->changeFrame(localRobot, localRobot->getEndEffector(gp.grasp.eefName)->getGCP()->getName());
        //        desiredTCPPose = FramedPosePtr::dynamicCast(gp.grasp.framedPose)->toGlobal(localRobot);
        auto desiredTCPPose = FramedPosePtr::dynamicCast(gp.grasp.framedPose)->toGlobalEigen(localRobot);
        auto desiredTCPPoseRelativeToRobotEigen = desiredRobotPoseEigen.inverse() * desiredTCPPose;
        FramedPosePtr desiredTCPPoseRelativeToRobot {new FramedPose(desiredTCPPoseRelativeToRobotEigen, localRobot->getRootNode()->getName(), localRobot->getName())};

        std::string rnsToUse;
        auto tcpName = localRobot->getEndEffector(gp.grasp.eefName)->getTcp()->getName();
        for (const auto& rnsName : robotNodeSetNames)
        {
            ARMARX_CHECK_EXPRESSION_W_HINT(localRobot->hasRobotNodeSet(rnsName), "Could not find RNS '" << rnsName << "' in RNS list of robot " << localRobot->getName());
            if (localRobot->getRobotNodeSet(rnsName)->getTCP()->getName() == tcpName)
            {
                rnsToUse = rnsName;
                break;
            }
        }

        float visuSlowdownFactor =  getProperty<float>("VisualizationSlowdownFactor").getValue();


        if (rnsToUse.empty())
        {
            ARMARX_WARNING << "Could not find RNS with tcp '" << tcpName << "'; will not process the corresponding generated grasp...";
            continue;
        }

        if (/*rik->isFramedPoseReachable(rnsToUse, objectPoseRelativeToRobot) || */true)
        {
            if (robotVisuId.empty())
            {
                robotVisuId = newId();
                entityDrawer->setRobotVisu(layerName, robotVisuId, rsc->getRobotFilename(), boost::join(rsc->getArmarXPackages(), ","), CollisionModel);
                entityDrawer->updateRobotColor(layerName, robotVisuId, COLOR_ROBOT);
            }
            entityDrawer->updateRobotColor(layerName, robotVisuId, DrawColor {1.0, 1.0f, 1.0, 0.5});
            entityDrawer->updateRobotPose(layerName, robotVisuId, desiredRobotPose);
            entityDrawer->updateRobotConfig(layerName, robotVisuId, localRobot->getConfig()->getRobotNodeJointValueMap());
            ARMARX_VERBOSE << "Pose " << VAROUT(*desiredTCPPoseRelativeToRobot) << " with RNS '" << rnsToUse << "' is reachable";
            auto desiredRobotConfig = rik->computeIKFramedPose(rnsToUse, desiredTCPPoseRelativeToRobot, eAll);
            if (desiredRobotConfig.empty())
            {
                usleep(1000000 * visuSlowdownFactor);
                visualizeGrasp(gp.grasp, i, DrawColor {1.0, 0, 0, 1});
                entityDrawer->updateRobotColor(layerName, robotVisuId, DrawColor {1.0, 0.0f, 0.0, 0.5});
                ARMARX_VERBOSE << "...but has no IK solution";
                usleep(1000000 * visuSlowdownFactor);
                entityDrawer->removeObjectVisu(graspVisuId.first, graspVisuId.second);
                continue;
            }


            ARMARX_VERBOSE << "... and has an IK solution";
            usleep(1000000 * visuSlowdownFactor);
            entityDrawer->updateRobotColor(layerName, robotVisuId, DrawColor {0.0, 1.0f, 0.0, 0.5});
            entityDrawer->updateRobotPose(layerName, robotVisuId, desiredRobotPose);
            entityDrawer->updateRobotConfig(layerName, robotVisuId, desiredRobotConfig);
            usleep(2000000 * visuSlowdownFactor);
            //            usleep(500000);

            for (auto it = currentConfig.begin(); it != currentConfig.end();)
            {
                if (desiredRobotConfig.find(it->first) == desiredRobotConfig.end())
                {
                    it = currentConfig.erase(it);
                }
                else
                {
                    ++it;
                }
            }

            for (const auto& entry : desiredRobotConfig)
            {
                if (currentConfig.find(entry.first) == currentConfig.end())
                {
                    ARMARX_INFO << "Current config: " << currentConfig;
                    ARMARX_INFO << "Desired config: " << desiredRobotConfig;
                    ARMARX_CHECK_EXPRESSION_W_HINT(false,
                                                   "calculated configuration contains a joint '" << entry.first << "' whose current value is unknown");
                }
            }
            ARMARX_CHECK_EXPRESSION(currentConfig.size() == desiredRobotConfig.size());
            ARMARX_CHECK_EXPRESSION(!desiredRobotConfig.empty());
            //            entityDrawer->setPoseVisu("Poses" , "TCPTargetPose" + std::to_string(i), objectPoseRelativeToRobot->toGlobal(rsc->getSynchronizedRobot()));
            mpdList.push_back({new FramedPose(currentRobotPose, GlobalFrame, ""),
                                  desiredRobotPose, currentConfig, desiredRobotConfig, rnsToUse, gp.grasp.eefName
            });
        }
        else
        {
            ARMARX_VERBOSE << "Pose " << VAROUT(*desiredTCPPoseRelativeToRobot) << " with RNS '" << rnsToUse << "' not reachable";
        }
        entityDrawer->removeObjectVisu(graspVisuId.first, graspVisuId.second);
        i++;
    }
    entityDrawer->removeRobotVisu(layerName, robotVisuId);
    return mpdList;
}

GraspingTrajectory GraspingManager::generateGraspingTrajectory(const std::string& objectInstanceEntityId, const Ice::Current&)
{
    ARMARX_ON_SCOPE_EXIT
    {
        resetStepDescription();
    };
    float visuSlowdownFactor = getProperty<float>("VisualizationSlowdownFactor");
    RemoteRobot::synchronizeLocalClone(localRobot, rsc);
    entityDrawer->clearLayer(layerName);
    resetStepDescription();
    setNextStepDescription("Generating grasps", objectInstanceEntityId);
    auto grasps = generateGrasps(objectInstanceEntityId);
    setNextStepDescription("Filtering grasps", objectInstanceEntityId);

    auto filteredGrasps = filterGrasps(grasps);
    setNextStepDescription("Robot placement", objectInstanceEntityId);

    GraspingPlacementList graspPlacements = generateRobotPlacements(filteredGrasps, objectInstanceEntityId);

    //    std::vector<std::tuple<Eigen::Matrix4f, Eigen::Matrix4f, NameValueMap, NameValueMap, std::string>> sourceTargetConfigs;
    std::vector<MotionPlanningData> mpdList;
    setNextStepDescription("Calculating IK", objectInstanceEntityId);

    ARMARX_VERBOSE << "Step: check reachability / solve IK";
    mpdList = calculateIK(graspPlacements);
    if (mpdList.empty())
    {
        ARMARX_WARNING << "Step 'check reachability / solve IK' produced no valid results";
        return GraspingTrajectory();
    }

    GraspingTrajectoryList result;
    setNextStepDescription("Planning motion", objectInstanceEntityId);

    for (auto& graspingData : mpdList)
    {
        try
        {
            result.push_back(planMotion(graspingData));

        }
        catch (...)
        {
            handleExceptions();
        }
        if (!result.empty())
        {
            break;
        }
    }
    if (result.empty())
    {
        throw LocalException("Could not find any valid solution");
    }
    drawTrajectory(result.front());

    sleep(2 * visuSlowdownFactor);
    entityDrawer->removeLayer(layerName);
    return result.front();
}
