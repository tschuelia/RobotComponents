/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::LaserScannerSelfLocalisation
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMARX_COMPONENT_RobotComponents_LaserScannerSelfLocalisation_H
#define ARMARX_COMPONENT_RobotComponents_LaserScannerSelfLocalisation_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/util/IceReportSkipper.h>
#include <MemoryX/interface/component/LongtermMemoryInterface.h>
#include <MemoryX/interface/component/WorkingMemoryInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
#include <MemoryX/libraries/memorytypes/entity/SimpleEntity.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotComponents/interface/components/LaserScannerSelfLocalisation.h>

#include <Eigen/Eigen>
#include <atomic>

namespace armarx
{
    struct LineSegment2Df
    {
        Eigen::Vector2f start;
        Eigen::Vector2f end;
    };

    struct ExtractedEdge
    {
        LineSegment2Df segment;
        Eigen::Vector2f* pointsBegin;
        Eigen::Vector2f* pointsEnd;
    };


    struct LaserScanData
    {
        Eigen::Matrix4f pose;
        LaserScannerInfo info;
        boost::shared_ptr<Mutex> mutex;
        LaserScan scan;
        std::vector<Eigen::Vector2f> points;
        std::vector<ExtractedEdge> edges;
    };

    struct ReportedVelocity
    {
        float dt;
        float x;
        float y;
        float theta;
    };

    /**
     * @class LaserScannerSelfLocalisationPropertyDefinitions
     * @brief
     */
    class LaserScannerSelfLocalisationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        LaserScannerSelfLocalisationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ReportTopicName", "LaserScannerSelfLocalisationTopic", "The name of the report topic.");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "The name of the RobotStateComponent. Used to get local transformation of laser scanners");
            defineOptionalProperty<std::string>("PlatformName", "Platform", "Name of the platform to use. This property is used to listen to the platform topic");
            defineOptionalProperty<std::string>("LaserScannerUnitName", "LaserScannerSimulation", "Name of the laser scanner unit to use.");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory that should be used");
            defineOptionalProperty<std::string>("LongtermMemoryName", "LongtermMemory", "Name of the LongtermMemory component");
            defineOptionalProperty<bool>("UpdateWorkingMemory", true, "Update the working memory with the corrected position (disable in simulation)");
            defineOptionalProperty<std::string>("MapFilename", "RobotComponents/maps/building-5020-kitchen.json", "Floor map (2D) used for global localisation");
            defineOptionalProperty<std::string>("AgentName", "", "Name of the agent instance. If empty, the robot name of the RobotStateComponent will be used");

            defineOptionalProperty<int>("UpdatePeriodInMs", 5, "Update period used for the map localisation");
            defineOptionalProperty<int>("UpdatePeriodWorkingMemoryInMs", 30, "Update period used for updating the working memory");
            defineOptionalProperty<int>("UpdatePeriodLongtermMemoryInMs", 30, "Update period used for updating the longterm memory");
            defineOptionalProperty<float>("RobotPositionZ", 0.0f, "The z-coordinate of the reported postion. Laser scanners can only self localize in x,y.");

            defineOptionalProperty<int>("SmoothFrameSize", 7, "Frame size used for smoothing of laser scanner input", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<int>("SmoothMergeDistance", 160, "Distance in mm up to which laser scanner points are merged", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("MatchingMaxDistance", 300.0f, "Maximal distance in mm up to which points are matched against edges of the map", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("MatchingMinPoints", 0.01f, "Minimum percentage of points which need to be matched (range [0, 1]). If less points are matched no global correction is applied.", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("MatchingCorrectionFactor", 0.5f, "This factor is used to apply the calculated map correction (range [0, 1])", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("EdgeMaxDistance", 600.0f, "Maximum distance between adjacent points up to which they are merged into one edge [mm]", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("EdgeMaxDeltaAngle", 0.10472f, "Maximum angle delta up to which adjacent points are merged into one edge [rad]", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<float>("EdgePointAddingThreshold", 10.0f, "Maximum least square error up to which points are added to an edge (extension at the front and back)", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<int>("EdgeEpsilon", 4, "Half frame size for line regression (angle calculation)", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<int>("EdgeMinPoints", 30, "Minimum number of points per edge (no edges with less points will be extracted)", PropertyDefinitionBase::eModifiable);


            defineOptionalProperty<bool>("UseOdometry", true, "Enable or disable odometry for pose estimation", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("UseMapCorrection", true, "Enable or disable map localisation for pose correction", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("ReportPoints", false, "Enable or disable the reports of (post-processed) laser scan points", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<bool>("ReportEdges", true, "Enable or disable the reports of extracted edges", PropertyDefinitionBase::eModifiable);
        }
    };

    /**
     * @defgroup Component-LaserScannerSelfLocalisation LaserScannerSelfLocalisation
     * @ingroup RobotComponents-Components
     * The component LaserScannerSelfLocalisation is responsible for self localisation of a
     * platform in a predefined two dimensional map.
     *
     * The odometry is used as a primary source for determining the current position of the
     * platform. Since this method leads to unbounded errors, we also use the laser scanners
     * to correct the estimated position. This is done by matching the laser scan input to
     * edges of a predefined map.
     *
     * This method requires a good estimate of the initial position at startup because the
     * correction by the laser scans can only correct small errors.
     *
     *
     * @class LaserScannerSelfLocalisation
     * @ingroup Component-LaserScannerSelfLocalisation
     * @brief The class LaserScannerSelfLocalisation implements a self localisation strategy.
     */
    class LaserScannerSelfLocalisation :
        virtual public armarx::Component,
        virtual public armarx::LaserScannerSelfLocalisationInterface
    {
    public:
        LaserScannerSelfLocalisation();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override
        {
            return "LaserScannerSelfLocalisation";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // LaserScannerSelfLocalisationInterface interface
        std::string getReportTopicName(const Ice::Current&);
        void setAbsolutePose(Ice::Float x, Ice::Float y, Ice::Float theta, const Ice::Current&);
        LineSegment2DSeq getMap(const Ice::Current&);

        // LaserScannerUnitListener interface
        void reportSensorValues(const std::string& device, const std::string& name, const LaserScan& scan,
                                const TimestampBasePtr& timestamp, const Ice::Current&) override;

        // PlatformUnitListener interface
        void reportPlatformPose(Ice::Float x, Ice::Float y, Ice::Float theta, const Ice::Current&) override;
        void reportNewTargetPose(Ice::Float x, Ice::Float y, Ice::Float theta, const Ice::Current&) override;
        void reportPlatformVelocity(Ice::Float velX, Ice::Float velY, Ice::Float velTheta, const Ice::Current&) override;

        // Component interface
        void icePropertiesUpdated(const std::set<std::string>& changedProperties);

    private:
        void updateLocalisation();

        void updateProperties(bool initial = false);

    private:
        std::string reportTopicName;
        std::string robotStateComponentName;
        std::string platformName;
        std::string laserScannerUnitName;
        std::string workingMemoryName;
        std::string longtermMemoryName;
        std::string mapFilename;
        std::string agentName;
        float workingMemoryUpdateFrequency;
        float longtermMemoryUpdateFrequency;
        float robotPositionZ = 0.0f;

        Mutex propertyMutex;
        int   propSmoothFrameSize;
        int   propSmoothMergeDistance;
        float propMatchingMaxDistance;
        float propMatchingMinPoints;
        float propMatchingCorrectionFactor;
        float propEdgeMaxDistance;
        float propEdgeMaxDeltaAngle;
        float propEdgePointAddingThreshold;
        int   propEdgeEpsilon;
        int   propEdgeMinPoints;
        bool  propReportPoints;
        bool  propReportEdges;
        bool  propUseOdometry;
        bool  propUseMapCorrection;

        std::atomic<bool>  useOdometry;

        std::vector<LineSegment2Df> map;

        RobotStateComponentInterfacePrx robotState;
        LaserScannerUnitInterfacePrx laserScannerUnit;
        LaserScannerSelfLocalisationListenerPrx reportTopic;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::AgentInstancesSegmentBasePrx agentsMemory;
        memoryx::AgentInstancePtr agentInstance;
        bool updateWorkingMemory = false;
        memoryx::LongtermMemoryInterfacePrx longtermMemory;
        memoryx::PersistentEntitySegmentBasePrx selfLocalisationSegment;
        memoryx::SimpleEntityPtr poseEntity;

        std::vector<LaserScanData> scanData;

        Mutex setPoseMutex;
        boost::optional<Eigen::Vector3f> setPose;

        Mutex odometryMutex;
        std::vector<ReportedVelocity> reportedVelocities;
        Eigen::Vector3f lastVelocity;
        IceUtil::Time lastVelocityUpdate;

        Eigen::Vector3f estimatedPose;

        PeriodicTask<LaserScannerSelfLocalisation>::pointer_type task;
        IceReportSkipper s;
    };
}

#endif
