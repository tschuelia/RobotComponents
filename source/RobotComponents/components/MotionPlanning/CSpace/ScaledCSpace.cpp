/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <algorithm>
#include "ScaledCSpace.h"

namespace armarx
{

    ScaledCSpace::ScaledCSpace(const CSpaceBasePtr& cspace, const Ice::FloatSeq& scale)
    {
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cspace);
        this->originalCSpace = cspace;
        this->scalingFactors = scale;
        unscaled.resize(scalingFactors.size());

        if (static_cast<std::size_t>(originalCSpace->getDimensionality()) != scalingFactors.size())
        {
            std::stringstream s;
            s << "Different number of scaling factors (" << scalingFactors.size()
              << ") than dimensionality of the original cspace(" << originalCSpace->getDimensionality() << ")";
            ARMARX_ERROR_S << s.str();
            throw std::invalid_argument {s.str()};
        }

        if (std::any_of(scalingFactors.begin(), scalingFactors.end(), [](float f)
    {
        return f <= 0.f;
    }))
        {
            ARMARX_ERROR_S << "One or more factors are <= 0!";
            throw std::invalid_argument {"One or more factors are <= 0!"};
        }
    }

    void ScaledCSpace::unscaleConfig(VectorXf& config) const
    {
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(config.size() == scalingFactors.size());
        unscaleToBuffer(config.data(), config);
    }

    void ScaledCSpace::unscalePath(Path& path) const
    {
        unscalePath(path.nodes);
    }

    void ScaledCSpace::unscalePath(PathWithCost& path) const
    {
        unscalePath(path.nodes);
    }

    void ScaledCSpace::unscalePath(VectorXfSeq& nodes) const
    {
        for (auto & cfg : nodes)
        {
            unscaleConfig(cfg);
        }
    }

    void ScaledCSpace::scaleConfig(VectorXf& config) const
    {
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(config.size() == scalingFactors.size());
        for (std::size_t i = 0; i < scalingFactors.size(); ++i)
        {
            config.at(i) *= scalingFactors.at(i);
        }
    }

    bool ScaledCSpace::isCollisionFree(const::std::pair<const Ice::Float*, const Ice::Float*>& cfg, const Ice::Current&)
    {
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(unscaled.size() == static_cast<std::size_t>(std::distance(cfg.first, cfg.second)));
        unscaleToBuffer(cfg.first, unscaled);
        return originalCSpace->isCollisionFree(std::make_pair(unscaled.data(), unscaled.data() + unscaled.size()));
    }

    CSpaceBasePtr ScaledCSpace::clone(const Ice::Current&)
    {
        ScaledCSpacePtr cloned {new ScaledCSpace{}};
        cloned->scalingFactors = scalingFactors;
        cloned->originalCSpace = originalCSpace->clone();
        return cloned;
    }

    FloatRangeSeq ScaledCSpace::getDimensionsBounds(const Ice::Current&) const
    {
        FloatRangeSeq bounds = originalCSpace->getDimensionsBounds();

        for (std::size_t i = 0; i < scalingFactors.size(); ++i)
        {
            bounds.at(i).min *= scalingFactors.at(i);
            bounds.at(i).max *= scalingFactors.at(i);
        }

        return bounds;
    }

    void ScaledCSpace::unscaleToBuffer(const Ice::Float* cfg, VectorXf& buffer) const
    {
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(buffer.size() == scalingFactors.size());

        for (std::size_t i = 0; i < scalingFactors.size(); ++i)
        {
            buffer.at(i) = cfg[i] / scalingFactors.at(i);
        }
    }
}
