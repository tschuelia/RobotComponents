/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_LIBRARY_RobotComponents_PlanningAlgorithms_AStar_Task_H
#define _ARMARX_LIBRARY_RobotComponents_PlanningAlgorithms_AStar_Task_H

#include <mutex>
#include <chrono>
#include<atomic>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>
#include <ArmarXCore/interface/core/RemoteObjectNode.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AStar/Task.h>
#include "../../util/Metrics.h"

#include "../MotionPlanningTask.h"

namespace armarx
{
    namespace astar
    {
        class Task;
        typedef IceInternal::Handle<Task> TaskPtr;

        class Task:
            public virtual MotionPlanningTaskWithDefaultMembers,
            public virtual TaskBase
        {
        public:
            /**
            * @brief A task using the A* algorithm.
            * @param cspace The used cspace.
            * @param startCfg The start configuration.
            * @param goalCfg The goal configuration.
            * @param dcdStep The step size used for discrete collision checking.
            * @param maximalPlanningTimeInSeconds The maximal planning time in seconds.
            */
            Task(//problem
                const CSpaceBasePtr& cspace,
                const VectorXf& startCfg,
                const VectorXf& goalCfg,
                const std::string& taskName = "AStarTask",
                //general
                float dcdStep = 0.01f,
                float gridStepSize = 2.5f,
                Ice::Long maximalPlanningTimeInSeconds = 300
            );

            //PlanningControlInterface
            /**
            * @brief Aborts planning.
            */
            virtual void abortTask(const Ice::Current& = Ice::Current()) override
            {
                taskIsAborted = true;
            }
            /**
            * @return The found path. (empty if no path is found)
            */
            virtual Path getPath(const Ice::Current& = Ice::Current()) const override;


            //PlanningTaskBase
            /**
            * @brief Runs the task.
            * @param remoteNodes The list of \ref RemoteObjectNodeInterfacePrx used to distribute work to computers.
            */
            virtual void run(const RemoteObjectNodePrxList&, const Ice::Current& = Ice::Current()) override;

            bool isPathCollisionFree(const VectorXf& from, const VectorXf& to);
        protected:
            /**
            * @brief Ctor used by object factories.
            */
            Task() = default;

            mutable std::mutex mtx;
            Path solution = {{}, "Path"};
            std::atomic_bool taskIsAborted {false};
        private:
            template<class Base, class Derived> friend class ::armarx::GenericFactory;
        };
    }
    using AStarTask = astar::Task;
    using AStarTaskPtr = IceUtil::Handle<AStarTask>;
    using AStarTaskHandle = RemoteHandle<MotionPlanningTaskControlInterfacePrx>;
}
#endif
