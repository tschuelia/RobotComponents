/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_LIBRARY_RobotComponents_PlanningAlgorithms_AdaptiveDynamicDomainInformedRRTStar_Task_H
#define _ARMARX_LIBRARY_RobotComponents_PlanningAlgorithms_AdaptiveDynamicDomainInformedRRTStar_Task_H

#include <mutex>
#include <atomic>
#include <condition_variable>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/Task.h>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/ManagerNode.h>
#include "../../ResourceRequestStrategies/ComputingPowerRequestStrategy.h"
#include "../../util/PlanningUtil.h"

#include "util.h"
#include "../CPRSAwareMotionPlanningTask.h"

namespace armarx
{
    namespace addirrtstar
    {
        class Task;
        /**
        * @brief An ice handle for an addirrt* \ref Task
        */
        typedef IceInternal::Handle<Task> TaskPtr;

        /**
        * @brief An addirrt* task.
        * The olanning algorithm used is a combination of:
        *  - bulk distributed rrt.
        *  - informed rrt*
        *  - adaptive dynamic domain
        *
        * bulks and batches are used as synonyms.
        */
        class Task:
            public virtual cprs::CPRSAwareMotionPlanningTask,
            public virtual TaskBase,
            public virtual MotionPlanningMultiPathWithCostTaskCI
        {
        public:
            /**
            * @brief Ctor.
            * @param cspace The planning cspace.
            * @param planningComputingPowerRequestStrategy  The used cprs.
            * @param startCfg The start configuration.
            * @param goalCfg The goal configuration.
            * @param addParams The parameters for adaptive dynamic domain.
            * @param targetCost The target cost. (planning stops when a path with a length <= was found)
            * @param dcdStep The dcd step size.
            * @param maximalPlanningTimeInSeconds The maximal planning time in seconds. (planning will stop after this time)
            * @param batchSize The size of a batch.
            * @param nodeCountDeltaForGoalConnectionTries Number of nodes created (by a worker) before a connect to the goal node is tried (by this worker).
            * @param initialWorkerCount The in itaial number of worker processes.
            * @param maximalWorkerCount The maximal number of worker processes.
            */
            Task(//problem
                CSpaceBasePtr cspace,
                const cprs::ComputingPowerRequestStrategyBasePtr& planningComputingPowerRequestStrategy,
                VectorXf startCfg,
                VectorXf goalCfg,
                const std::string& taskName = "ADDIRRTStarTask",
                Ice::Long maximalPlanningTimeInSeconds = 300,
                AdaptiveDynamicDomainParameters addParams = generateADDParamsFromDCDStepsize(0.01f),
                float targetCost = 0,
                //general
                float dcdStep = 0.01f,
                Ice::Long batchSize = 10,
                Ice::Long nodeCountDeltaForGoalConnectionTries = 50,
                //management
                Ice::Long initialWorkerCount = 1,
                Ice::Long maximalWorkerCount = std::numeric_limits<Ice::Long>::max()
            );


            virtual PathWithCost getPathWithCost(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getPathWithCost();
            }
            virtual Path getNthPath(Ice::Long n, const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningMultiPathWithCostTaskCI::getNthPath(n);
            }

            virtual Path getPath(const Ice::Current& = Ice::Current()) const override
            {
                return MotionPlanningWithCostTaskCI::getPath();
            }

            //TaskControlInterface
            /**
            * @return The shortest found path. (with its cost)
            */
            virtual PathWithCost getBestPath(const Ice::Current& = Ice::Current()) const override;
            /**
            * @return The number of found paths.
            */
            virtual Ice::Long getPathCount(const Ice::Current& = Ice::Current()) const override;
            /**
            * @param index The index.
            * @return The path at the given index.
            */
            virtual PathWithCost getNthPathWithCost(Ice::Long index, const Ice::Current& = Ice::Current()) const override;
            /**
            * @return All found paths.
            */
            virtual PathWithCostSeq getAllPathsWithCost(const Ice::Current& = Ice::Current()) const override;

            //PlanningControlInterface
            /**
            * @brief Aborts the task.
            */
            virtual void abortTask(const Ice::Current& = Ice::Current()) override;

            //PlanningTaskBase
            /**
            * @brief Runs the task.
            * @param remoteNodes The list of \ref RemoteObjectNodeInterfacePrx used to distribute work to computers.
            */
            virtual void run(const RemoteObjectNodePrxList& remoteNodes, const Ice::Current& = Ice::Current()) override;

            /**
            * @brief Used by the manager to store its found paths.
            * @param newPathList The paths.
            */
            virtual void setPaths(const PathWithCostSeq& newPathList, const Ice::Current& = Ice::Current()) override;

            /**
            * @return The current node count.
            */
            virtual Ice::Long getNodeCount(const Ice::Current& = Ice::Current()) const override;

            // ResourceManagementInterface interface
            virtual void setMaxCpus(Ice::Int maxCpus, const Ice::Current& = Ice::Current()) override;

            virtual Ice::Int getMaxCpus(const Ice::Current& = Ice::Current()) const override;

        protected:
            template<class Base, class Derived> friend class ::armarx::GenericFactory;

            /**
            * @brief Checks for illegal parameters
            */
            void checkParameters();

            /**
            * @brief Ctor used by object factories.
            */
            Task():
                TaskBase(),
                cachedNodeCount {0}
            {
            }

            /**
            * @brief Mutex to protect internal structures.
            */
            mutable std::recursive_mutex mutex;
            //we have to ensure that the waiting thread has only locked the recursive mutex once,
            //since the condition variable only will use the unlock method on the unique_lock once during the wait.
            //http://stackoverflow.com/questions/14323340/can-you-combine-stdrecursive-mutex-with-stdcondition-variable
            //
            //this is given for run
            /**
            * @brief CV used by the dispatcher thread to wait until planning is done.
            */
            std::condition_variable_any managerDone;

            /**
            * @brief The manager node.
            */
            RemoteHandle<ManagerNodeBasePrx> manager;
            /**
            * @brief All found paths
            */
            PathWithCostSeq paths;

            /**
            * @brief The cahced node count. The cache is filled when the manager node shuts down.
            */
            Ice::Long cachedNodeCount;
        };
    }
    using ADDIRRTStarTask = addirrtstar::Task;
    using ADDIRRTStarTaskPtr = IceUtil::Handle<ADDIRRTStarTask>;
    using ADDIRRTStarTaskHandle = RemoteHandle<armarx::MotionPlanningMultiPathWithCostTaskControlInterfacePrx>;
}
#endif

