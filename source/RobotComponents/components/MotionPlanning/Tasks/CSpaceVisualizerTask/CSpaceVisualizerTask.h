/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef CSPACEVISUALIZERTASK_H
#define CSPACEVISUALIZERTASK_H

#include <RobotComponents/components/MotionPlanning/Tasks/MotionPlanningTask.h>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/CSpaceVisualizerTask.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>

namespace armarx
{
    /**
     * @brief The purpose of this task is solely to visualize a cspace, which can be inspected in the
     * CSpaceVisualizer GUI plugin.
     */
    class CSpaceVisualizerTask :
        public virtual MotionPlanningTaskWithDefaultMembers,
        public virtual CSpaceVisualizerTaskBase
    {
    public:
        CSpaceVisualizerTask(//problem
            const SimoxCSpaceWith2DPoseBasePtr& cspace,
            const VectorXf& robotPlatform2DPose,
            const std::string& taskName = "CSpaceVisualizerTask"
        );

        //PlanningControlInterface
        /**
        * @brief Does not do anything for this task.
        */
        virtual void abortTask(const Ice::Current& = Ice::Current()) override
        {

        }
        /**
        * @return Contains the robot pose from the constructor..
        */
        virtual Path getPath(const Ice::Current& = Ice::Current()) const override;


        //PlanningTaskBase
        /**
        * @brief Does not do anything for this task.
        */
        virtual void run(const RemoteObjectNodePrxList&, const Ice::Current& = Ice::Current()) override;
    protected:
        /**
        * @brief Ctor used by object factories.
        */
        CSpaceVisualizerTask() = default;
    private:
        template<class Base, class Derived> friend class ::armarx::GenericFactory;

        // Object interface
    public:
        void ice_postUnmarshal() override;
    };
    using CSpaceVisualizerTaskPtr = IceUtil::Handle<CSpaceVisualizerTask>;
    using CSpaceVisualizerTaskHandle = RemoteHandle<MotionPlanningTaskControlInterfacePrx>;

}
#endif
