/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Task.h"

#include "../../util/PlanningUtil.h"

namespace armarx
{
    namespace rrtconnect
    {
        Task::Task(const CSpaceBasePtr& cspace, const VectorXf& startCfg, const VectorXf& goalCfg, const std::string& taskName, Ice::Long maximalPlanningTimeInSeconds, float dcdStep, Ice::Long workerCount)
        {
            this->cspace = cspace;
            this->startCfg = startCfg;
            this->goalCfg = goalCfg;
            this->dcdStep = dcdStep;
            this->maximalPlanningTimeInSeconds = maximalPlanningTimeInSeconds;
            this->workerCount = workerCount;
            this->taskName = taskName;
        }

        void Task::abortTask(const Ice::Current&)
        {
            std::lock_guard<std::mutex> lock {mtx};
            setTaskStatus((getTaskStatus() != TaskStatus::eDone) ? TaskStatus::ePlanningAborted : TaskStatus::eDone);
            //worker will be aborted in run()
            doneCV.notify_all();
        }

        Path Task::getPath(const Ice::Current&) const
        {
            std::lock_guard<std::mutex> lock {mtx};
            return solution;
        }

        void Task::workerHasAborted(Ice::Long workerId, const Ice::Current&)
        {
            std::lock_guard<std::mutex> lock {mtx};
            ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(static_cast<std::size_t>(workerId) < workerAbortedFlags.size());
            workerAbortedFlags.at(workerId) = true;
        }

        void Task::run(const RemoteObjectNodePrxList& remoteNodes, const Ice::Current&)
        {
            ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cspace);
            ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(std::all_of(remoteNodes.begin(), remoteNodes.end(), [](const RemoteObjectNodeInterfacePrx & n)
            {
                return n;
            }));
            //init
            workerAbortedFlags.assign(workerCount, false);
            setTaskStatus(TaskStatus::ePlanning);
            const std::string topicName = "RRTConnectUpdateTopic:" + getProxy()->ice_getIdentity().name;
            //check trivial cases

            const auto startIsCollisionFree = cspace->isCollisionFree(std::pair<const Ice::Float*, const Ice::Float*> {startCfg.data(), startCfg.data() + startCfg.size()});
            if (!startIsCollisionFree)
            {
                setTaskStatus(TaskStatus::ePlanningFailed);
                ARMARX_WARNING << "MotionPlanningTask " << taskName << " trivially failed! start cfg in collision. start:\n" << startCfg;
                return;
            }

            const auto  goalIscollisionFree = cspace->isCollisionFree(std::pair<const Ice::Float*, const Ice::Float*> { goalCfg.data(), goalCfg.data() + goalCfg.size()});
            if (!goalIscollisionFree)
            {
                setTaskStatus(TaskStatus::ePlanningFailed);
                ARMARX_WARNING << "MotionPlanningTask " << taskName << " trivially failed! goal cfg in collision. goal:\n" << goalCfg;
                return;
            }

            if (euclideanDistance(startCfg.begin(), startCfg.end(), goalCfg.begin()) <= dcdStep)
            {
                solution.nodes.emplace_back(startCfg);
                solution.nodes.emplace_back(goalCfg);
                solution.pathName = taskName + "_trivialPath_" + ice_staticId();
                ARMARX_WARNING << "MotionPlanningTask " << taskName << " trivially succeeded! (start and goal less than dcd step size apart!)";
                setTaskStatus(TaskStatus::eDone);
                return;
            }

            //start workers
            std::unique_lock<std::mutex> lock {mtx};
            std::vector<RemoteHandle<WorkerNodeBasePrx>> workers;
            WorkerNodeBasePrxList workerProxies;

            workers.reserve(workerCount);
            auto workerToStart = workerCount;
            const auto taskName = getProxy()->ice_getIdentity().name;
            for (auto & ron : remoteNodes)
            {
                auto maxWorkerForNode = ron->getNumberOfCores();
                for (Ice::Long i = 0; i < maxWorkerForNode && workerToStart; ++i)
                {
                    WorkerNodeBasePtr worker {new WorkerNode{
                            cspace->clone(),
                            startCfg,
                            goalCfg,
                            dcdStep,
                            TaskBasePrx::uncheckedCast(getProxy()),
                            topicName,
                            static_cast<Ice::Long>(workers.size()),
                            workerCount
                        }
                    };
                    const std::string workerName = ManagedIceObject::generateSubObjectName(taskName, "RRTConnectWorker:" + std::to_string(workers.size()));
                    workers.emplace_back(ron->registerRemoteHandledObject(workerName, worker));
                    workerProxies.emplace_back(*(workers.back()));
                    --workerToStart;
                }
                if (!workerToStart)
                {
                    break;
                }
            }

            //distribute worker proxy
            for (auto & w : workers)
            {
                w->setWorkerNodes(workerProxies);
            }

            //wait for done or timeout
            const auto endTime = std::chrono::system_clock::now() + std::chrono::seconds {maximalPlanningTimeInSeconds};

            while (true)
            {
                doneCV.wait_for(lock, std::chrono::milliseconds {10});
                if (endTime < std::chrono::system_clock::now())
                {
                    setTaskStatus(TaskStatus::ePlanningFailed);
                    break;
                }
                if (getTaskStatus() != TaskStatus::ePlanning)
                {
                    break;
                }
            }

            //wait for all stopped
            while (std::any_of(workerAbortedFlags.begin(), workerAbortedFlags.end(), [](bool b)
        {
            return !b;
        }))
            {
                for (auto & w : workers)
                {
                    w->abort();
                }
                doneCV.wait_for(lock, std::chrono::milliseconds {10});
            }
        }

        void Task::setPath(const Path& path, const Ice::Current&)
        {
            std::lock_guard<std::mutex> lock {mtx};
            solution = path;
            solution.pathName = taskName + "_path_" + ice_staticId();
            setTaskStatus(TaskStatus::eDone);
            doneCV.notify_all();
        }

        void Task::postEnqueueing()
        {
            MotionPlanningTaskWithDefaultMembers::postEnqueueing();
            cspace->initCollisionTest();
        }
    }
}
