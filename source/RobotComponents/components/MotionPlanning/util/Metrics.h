/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_LIBRARY_RobotComponents_util_Metrics_H
#define _ARMARX_LIBRARY_RobotComponents_util_Metrics_H

#include <cmath>
#include <iterator>

namespace armarx
{
    /**
     * @brief Returns the squared euclidean distance with weighted with the given vector.
     * @param first1 First value of the first vector.
     * @param last1 One past the last  value of the first vector.
     * @param first2 First value of the second vector.
     * @param firstw First weight.
     * @return The squared euclidean distance with weighted with the given vector.
     */
    template<class IteratorType1, class IteratorType2, class IteratorType3>
    float    euclideanDistanceWeightedSquared(IteratorType1 first1, IteratorType1 last1, IteratorType2 first2, IteratorType3 firstw)
    {
        float akk = 0;

        for (; first1 < last1; ++first1, ++first2, ++firstw)
        {
            akk += std::pow((*first1 - *first2) * (*firstw), 2);
        }

        return akk;
    }

    /**
     * @brief Returns the squared euclidean distance.
     * @param first1 First value of the first vector.
     * @param last1 One past the last  value of the first vector.
     * @param first2 First value of the second vector.
     * @return The squared euclidean distance.
     */
    template<class IteratorType1, class IteratorType2>
    float euclideanDistanceSquared(IteratorType1 first1, IteratorType1 last1, IteratorType2 first2)
    {
        float akk = 0;

        for (; first1 < last1; ++first1, ++first2)
        {
            akk += std::pow(*first1 - *first2, 2);
        }

        return akk;
    }

    /**
     * @brief Returns the euclidean distance with weighted with the given vector.
     * @param first1 First value of the first vector.
     * @param last1 One past the last  value of the first vector.
     * @param first2 First value of the second vector.
     * @param firstw First weight.
     * @return The euclidean distance with weighted with the given vector.
     */
    template<class IteratorType>
    float euclideanDistanceWeighted(IteratorType first1, IteratorType last1, IteratorType first2, IteratorType firstw)
    {
        return std::sqrt(euclideanDistanceWeightedSquared(first1, last1, first2, firstw));
    }

    /**
     * @brief Returns the euclidean distance.
     * @param first1 First value of the first vector.
     * @param last1 One past the last  value of the first vector.
     * @param first2 First value of the second vector.
     * @return The euclidean distance.
     */
    template<class IteratorType1, class IteratorType2>
    float euclideanDistance(IteratorType1 first1, IteratorType1 last1, IteratorType2 first2)
    {
        return std::sqrt(euclideanDistanceSquared(first1, last1, first2));
    }
}
#endif
