/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::PlannedMotionProvider
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlannedMotionProvider.h"
#include <VirtualRobot/Nodes/RobotNode.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotComponents/components/MotionPlanning/Tasks/RRTConnect/Task.h>
//#include <RobotComponents/components/MotionPlanning/Tasks/BiRRT/Task.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.h>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
#include <RobotComponents/components/MotionPlanning/CSpace/ScaledCSpace.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>


namespace armarx
{
    GraspingTrajectory PlannedMotionProvider::planMotion(const SimoxCSpaceBasePtr& cSpaceBase, const SimoxCSpaceBasePtr& cspacePlatformBase, const MotionPlanningData& mpd, const Ice::Current& c)
    {
        SimoxCSpacePtr cSpace = SimoxCSpacePtr::dynamicCast(cSpaceBase);
        cSpace->setStationaryObjectMargin(15);
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cSpace);
        SimoxCSpacePtr cSpacePlatform = SimoxCSpacePtr::dynamicCast(cspacePlatformBase);
        cSpacePlatform->setStationaryObjectMargin(50);
        ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(cSpacePlatform);

        Ice::FloatSeq cspaceScaling;
        for (VirtualRobot::RobotNodePtr node : localRobot->getRobotNodeSet(mpd.rnsToUse)->getAllRobotNodes())
        {
            cspaceScaling.push_back(node->isTranslationalJoint() ? 0.001 : 1);
        }
        ScaledCSpacePtr scaledJointCSpace = new ScaledCSpace(cSpace, cspaceScaling);
        armarx::VectorXf startCfg = cSpace->jointMapToVector(mpd.configStart);
        armarx::VectorXf goalCfg = cSpace->jointMapToVector(mpd.configGoal);
        scaledJointCSpace->scaleConfig(startCfg);
        scaledJointCSpace->scaleConfig(goalCfg);


        float dcdStep = 0.1;
        int coreCount = 4;
        ARMARX_VERBOSE << VAROUT(startCfg) << VAROUT(goalCfg);

        ARMARX_VERBOSE << "Planning joint trajectory using RRT";
        MotionPlanningTaskBasePtr rrt = new RRTConnectTask(scaledJointCSpace, startCfg, goalCfg, getDefaultName() + "JointRRT" + IceUtil::generateUUID(), 10, dcdStep, coreCount);
        //        MotionPlanningTaskBasePtr rrt = new birrt::Task(cSpace, startCfg, goalCfg, getDefaultName() + "JointBiRRT" + IceUtil::generateUUID(), dcdStep);

        RandomShortcutPostprocessorTaskHandle rsppHandle = mps->enqueueTask(new RandomShortcutPostprocessorTask(rrt, "JointRRTSmoothing" + IceUtil::generateUUID(), 10, dcdStep));
        planningTasks.push_back(rsppHandle);

        rsppHandle->waitForFinishedRunning();
        ARMARX_IMPORTANT << "joint trajectory planning took " << IceUtil::Time::microSeconds(rsppHandle->getRunningTime()).toMilliSecondsDouble() << " ms";

        if (rsppHandle->getTaskStatus() == armarx::TaskStatus::ePlanningFailed)
        {
            throw RuntimeError("Arm Motion Planning failed!");
        }

        Eigen::Vector3f rpy;
        VirtualRobot::MathTools::eigen4f2rpy(PosePtr::dynamicCast(mpd.globalPoseStart)->toEigen(), rpy);
        armarx::VectorXf startPos {mpd.globalPoseStart->position->x, mpd.globalPoseStart->position->y, rpy(2)};
        VirtualRobot::MathTools::eigen4f2rpy(PosePtr::dynamicCast(mpd.globalPoseGoal)->toEigen(), rpy);
        armarx::VectorXf goalPos {mpd.globalPoseGoal->position->x, mpd.globalPoseGoal->position->y, rpy(2)};
        ARMARX_INFO << VAROUT(startPos) << VAROUT(goalPos);

        ScaledCSpacePtr scaledPlatformCSpace = new ScaledCSpace(cSpacePlatform, {0.001, 0.001, 1});
        scaledPlatformCSpace->scaleConfig(startPos);
        scaledPlatformCSpace->scaleConfig(goalPos);

        MotionPlanningTaskBasePtr taskPlatformRRT = new armarx::RRTConnectTask {scaledPlatformCSpace, startPos, goalPos, getDefaultName() + "RRT" + IceUtil::generateUUID(), 60, dcdStep, coreCount};
        RandomShortcutPostprocessorTaskHandle rsppHandlePlatform = mps->enqueueTask(new RandomShortcutPostprocessorTask(taskPlatformRRT, "PlatformRRTSmoothing" + IceUtil::generateUUID(), 10, dcdStep));
        planningTasks.push_back(rsppHandlePlatform);

        rsppHandlePlatform->waitForFinishedRunning();
        ARMARX_IMPORTANT << "platform trajectory planning took " << IceUtil::Time::microSeconds(rsppHandlePlatform->getRunningTime()).toMilliSecondsDouble() << " ms";

        if (rsppHandlePlatform->getTaskStatus() == armarx::TaskStatus::ePlanningFailed)
        {
            throw RuntimeError("Platform Motion Planning failed!");
        }

        ARMARX_INFO << "Joint Motion Planning " << ((bool)((int)(rsppHandle->getTaskStatus()) == (int)(armarx::TaskStatus::ePlanningFailed)) ? "failed" : "succeeded");
        ARMARX_INFO << "Joint Motion Planning status: " << rsppHandle->getTaskStatus();

        ARMARX_INFO << "RRTConnectTask Planning " << ((bool)(rsppHandlePlatform->getTaskStatus() == armarx::TaskStatus::ePlanningFailed) ? "failed" : "succeeded");
        ARMARX_INFO << "RRTConnectTask Planning status: " << rsppHandlePlatform->getTaskStatus();


        auto jointTrajectoryPath = rsppHandle->getPath();
        auto posTrajectoryPath = rsppHandlePlatform->getPath();
        scaledPlatformCSpace->unscalePath(posTrajectoryPath);

        //    TrajectoryPtr posTrajectory(new Trajectory(posTrajectoryPath.nodes, Ice::DoubleSeq {}, {"x", "y", "alpha"}));
        //TODO check if solution found
        return {cSpacePlatform->pathToTrajectory(posTrajectoryPath),
                cSpace->pathToTrajectory(jointTrajectoryPath), mpd.rnsToUse, mpd.endeffector
               };
    }

    void PlannedMotionProvider::onInitComponent()
    {
        usingProxy("MotionPlanningServer");
        usingProxy("RobotStateComponent");
    }


    void PlannedMotionProvider::onConnectComponent()
    {
        assignProxy(mps, "MotionPlanningServer");
        assignProxy(robotStateComponent, "RobotStateComponent");
        localRobot = RemoteRobot::createLocalCloneFromFile(robotStateComponent, VirtualRobot::RobotIO::eStructure);
    }


    void PlannedMotionProvider::onDisconnectComponent()
    {
        planningTasks.clear();
    }


    void PlannedMotionProvider::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr PlannedMotionProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new PlannedMotionProviderPropertyDefinitions(
                getConfigIdentifier()));
    }
}
