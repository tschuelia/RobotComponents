/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::PlannedMotionProvider
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotComponents_PlannedMotionProvider_H
#define _ARMARX_COMPONENT_RobotComponents_PlannedMotionProvider_H


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/interface/components/PlannedMotionProviderInterface.h>
#include <RobotComponents/components/MotionPlanning/MotionPlanningObjectFactories.h>

namespace armarx
{
    /**
     * @class PlannedMotionProviderPropertyDefinitions
     * @brief
     */
    class PlannedMotionProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PlannedMotionProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-PlannedMotionProvider PlannedMotionProvider
     * @ingroup RobotComponents-Components
     * A description of the component PlannedMotionProvider.
     *
     * @class PlannedMotionProvider
     * @ingroup Component-PlannedMotionProvider
     * @brief Brief description of class PlannedMotionProvider.
     *
     * Detailed description of class PlannedMotionProvider.
     */
    class PlannedMotionProvider :
        virtual public armarx::Component,
        virtual public armarx::PlannedMotionProviderInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "PlannedMotionProvider";
        }

        GraspingTrajectory planMotion(const SimoxCSpaceBasePtr& cSpaceBase, const SimoxCSpaceBasePtr& cspacePlatformBase, const MotionPlanningData& mpd, const Ice::Current& c = Ice::Current());

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        std::vector<RemoteHandle<MotionPlanningTaskControlInterfacePrx>> planningTasks;
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
        MotionPlanningServerInterfacePrx mps;
    };
}

#endif
