/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::SimpleGraspGenerator
 * @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleGraspGenerator.h"
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>

using namespace armarx;
using namespace memoryx;

void SimpleGraspGenerator::onInitComponent()
{
    usingProxy("WorkingMemory");
    usingProxy("PriorKnowledge");

    offeringTopic("DebugDrawerUpdates");
}

void SimpleGraspGenerator::onConnectComponent()
{
    assignProxy(wm, "WorkingMemory");
    assignProxy(prior, "PriorKnowledge");
    objectInstances = wm->getObjectInstancesSegment();
    objectClasses = prior->getObjectClassesSegment();

    fileManager = memoryx::GridFileManagerPtr(new memoryx::GridFileManager(prior->getCommonStorage()));

    debugDrawerPrx = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
}

void SimpleGraspGenerator::onDisconnectComponent()
{
}

void SimpleGraspGenerator::onExitComponent()
{
}

GeneratedGraspList SimpleGraspGenerator::generateGrasps(const std::string& objectInstanceEntityId, const Ice::Current& c)
{
    GeneratedGraspList result;
    //    int counter = 0;

    ObjectInstancePtr instance = ObjectInstancePtr::dynamicCast(objectInstances->getEntityById(objectInstanceEntityId));
    ARMARX_CHECK_EXPRESSION_W_HINT(instance, "no instance with id '" << objectInstanceEntityId << "'");

    ObjectClassPtr objectClass = ObjectClassPtr::dynamicCast(objectClasses->getEntityByName(instance->getMostProbableClass()));
    ARMARX_CHECK_EXPRESSION_W_HINT(objectClass, "no object class with name '" << instance->getMostProbableClass() << "' found ");
    memoryx::EntityWrappers::SimoxObjectWrapperPtr simoxWrapper = objectClass->addWrapper(new memoryx::EntityWrappers::SimoxObjectWrapper(fileManager));
    ARMARX_CHECK_EXPRESSION(simoxWrapper);
    VirtualRobot::ManipulationObjectPtr mo = simoxWrapper->getManipulationObject();
    ARMARX_CHECK_EXPRESSION(mo);


    auto objectFramedPose = instance->getPose();
    // localObjectPose is global, because objectFramedPose->frame is Global
    Eigen::Matrix4f localObjectPose = objectFramedPose->toEigen();


    // draw the position of the object
    //    Eigen::Vector3f tmpObjectPos = localObjectPose.block<3, 1>(0, 3);
    //    armarx::Vector3Ptr objectPos = new armarx::Vector3(tmpObjectPos);
    armarx::DrawColor objectColor;
    objectColor.r = 0;
    objectColor.g = 1;
    objectColor.b = 0;
    objectColor.a = 0.2f;

    //    debugDrawerPrx->setSphereDebugLayerVisu("objectSphere", objectPos, objectColor, 50.0f);

    std::string graspNameInfix  = getProperty<std::string>("GraspNameInfix");
    int countGrasps = 0;
    auto robotType =  getProperty<std::string>("RobotType").getValue();
    for (const VirtualRobot::GraspSetPtr& gs : mo->getAllGraspSets())
    {
        if (gs->getRobotType() != robotType)
        {
            continue;
        }
        for (const VirtualRobot::GraspPtr& g : gs->getGrasps())
        {
            ARMARX_INFO << "Found Grasp: " << g->getName() << " for eef: " << g->getEefName();
            if (!Contains(g->getName(), graspNameInfix, true))
            {
                ARMARX_INFO << "grasp name does not contain infix " << graspNameInfix << " - skipping it";
                continue;
            }
            auto localTcpPose = g->getTcpPoseGlobal(localObjectPose); // localTcpPose is global
            FramedPosePtr framedTcpPose {new FramedPose(localTcpPose, objectFramedPose->frame, objectFramedPose->agent)};

            result.push_back({1.f, g->getEefName(), framedTcpPose});

            countGrasps++;
        }
    }

    if (result.empty())
    {
        ARMARX_WARNING << "No grasps defined for object class '" << instance->getMostProbableClass() << "'";
    }
    ARMARX_INFO << "Found " << result.size() << " grasps";
    return result;
}
