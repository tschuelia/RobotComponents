/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::SimpleGraspGenerator
 * @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotComponents_SimpleGraspGenerator_H
#define _ARMARX_COMPONENT_RobotComponents_SimpleGraspGenerator_H

#include <ArmarXCore/core/Component.h>
#include <RobotComponents/interface/components/GraspingManager/GraspGeneratorInterface.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <MemoryX/interface/component/WorkingMemoryInterface.h>
#include <MemoryX/core/GridFileManager.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <Eigen/Geometry>

namespace armarx
{
    /**
     * @class SimpleGraspGeneratorPropertyDefinitions
     * @brief
     */
    class SimpleGraspGeneratorPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        SimpleGraspGeneratorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("GraspNameInfix", "Grasp", "All grasp candidates without this infix are filtered out. Case-insensitive.");
            defineOptionalProperty<float>("VisualizationSlowdownFactor", 1.0f, "1.0 is a good value for clear visualization, 0 the visualization should not slow down the process", PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<std::string>("RobotType", "Armar3", "Type of robot for which the grasps should be generated");

        }
    };

    /**
     * @defgroup Component-SimpleGraspGenerator SimpleGraspGenerator
     * @ingroup RobotComponents-Components
     * @brief The SimpleGraspGenerator component controls the head of the robot with inverse kinematics based on the uncertainty
     * of the current requested object locations.
     * The uncertainty of objects grow based on their motion model and the timed passed since the last localization.
     * It can be activated or deactivated with the Ice interface and given manual target positions to look at.
     */

    /**
     * @ingroup Component-SimpleGraspGenerator
     * @brief The SimpleGraspGenerator class
     */
    class SimpleGraspGenerator :
        virtual public Component,
        virtual public GraspGeneratorInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SimpleGraspGenerator";
        }

        GeneratedGraspList generateGrasps(const std::string& objectInstanceEntityId, const Ice::Current& c = Ice::Current());

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return PropertyDefinitionsPtr(new SimpleGraspGeneratorPropertyDefinitions(getConfigIdentifier()));
        }

    private:
        memoryx::WorkingMemoryInterfacePrx wm;
        memoryx::WorkingMemoryEntitySegmentBasePrx objectInstances;
        memoryx::PriorKnowledgeInterfacePrx prior;
        memoryx::PersistentObjectClassSegmentBasePrx objectClasses;

        memoryx::GridFileManagerPtr fileManager;

        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
    };
}

#endif
