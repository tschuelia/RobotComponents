armarx_component_set_name("SimpleRobotPlacement")


find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
if(Eigen3_FOUND)
    include_directories(${Eigen3_INCLUDE_DIR})
endif()


find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")
if(Simox_FOUND)
    include_directories(${Simox_INCLUDE_DIRS})
endif()

set(COMPONENT_LIBS
    RobotComponentsInterfaces
    MemoryXInterfaces MemoryXCore MemoryXVirtualRobotHelpers MemoryXObjectRecognitionHelpers
    RobotAPIInterfaces RobotAPICore
    ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart ArmarXCoreObservers MotionPlanning
    ${Simox_LIBRARIES}
)

set(SOURCES
./SimpleRobotPlacement.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)
set(HEADERS
./SimpleRobotPlacement.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
#add_subdirectory(test)
