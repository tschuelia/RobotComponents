/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2015-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::SimpleRobotPlacement
 * @author     Harry Arnst (harry dot arnst at student dot kit dot edu),
 *             Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleRobotPlacement.h"
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VirtualRobot/Grasping/Grasp.h>
#include <VirtualRobot/Workspace/Manipulability.h>
#include <VirtualRobot/ManipulationObject.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>
//#include <RobotComponents/components/MotionPlanning/CSpace/ScaledCSpace.h>
#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
//#include <RobotComponents/components/MotionPlanning/Tasks/RRTConnect/Task.h>
#include <IceUtil/UUID.h>
#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>


#include <math.h>

using namespace armarx;
using namespace memoryx;
using namespace VirtualRobot;

static const DrawColor COLOR_ROBOT
{
    1.0f, 1.0f, 0.5f, 0.3f
};

void SimpleRobotPlacement::onInitComponent()
{
    drawRobotId = "local_robot_";
    visuLayerName = "SimpleRobotPlacement";

    robotName = getProperty<std::string>("RobotName").getValue();
    robotFilePath = getProperty<std::string>("RobotFilePath").getValue();
    // retrieve absolute robot file path
    if (!ArmarXDataPath::getAbsolutePath(robotFilePath, robotFilePath))
    {
        ARMARX_ERROR << "Could not find robot file: " << robotFilePath;
    }

    workspaceFilePaths = armarx::Split(getProperty<std::string>("WorkspaceFilePaths").getValue(), ";");
    // retrieve absolute workspace file paths
    for (std::string& path : workspaceFilePaths)
    {
        std::string packageName = boost::filesystem::path {path} .begin()->string();
        ARMARX_CHECK_EXPRESSION_W_HINT(!packageName.empty(), "Workspace file path '" << path << "' could not be parsed correctly, because package name is empty");
        armarx::CMakePackageFinder project(packageName);
        path = project.getDataDir() + "/" + path;
        if (!boost::filesystem::exists(path))
        {
            throw LocalException("File not found at ") << path;
        }
    }

    colModel = getProperty<std::string>("CollisionModel").getValue();

    usingProxy("WorkingMemory");
    usingProxy("PriorKnowledge");

    usingProxy("RobotIK");
    usingProxy("RobotStateComponent");
    offeringTopic("DebugDrawerUpdates");
}

void SimpleRobotPlacement::onConnectComponent()
{
    assignProxy(wm, "WorkingMemory");
    assignProxy(prior, "PriorKnowledge");

    assignProxy(rik, "RobotIK");
    assignProxy(robotStateComponentPrx, "RobotStateComponent");

    objectInstances = wm->getObjectInstancesSegment();
    agentInstances = wm->getAgentInstancesSegment();
    objectClasses = prior->getObjectClassesSegment();

    fileManager = memoryx::GridFileManagerPtr(new memoryx::GridFileManager(prior->getCommonStorage()));

    debugDrawerPrx = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
    entityDrawerPrx = getTopic<memoryx::EntityDrawerInterfacePrx>("DebugDrawerUpdates");

    loadRobot();
    loadWorkspaces();
}

void SimpleRobotPlacement::onDisconnectComponent()
{
}

void SimpleRobotPlacement::onExitComponent()
{
}

GraspingPlacementList SimpleRobotPlacement::generateRobotPlacements(const GeneratedGraspList& grasps, const std::string& objectInstanceEntityId, const Ice::Current&)
{
    //    TIMING_START(RobotPlacement);
    planningTasks.clear();
    GraspingPlacementList result;
    AgentInstancePtr agent = AgentInstancePtr::dynamicCast(agentInstances->getAgentInstanceByName(robotName));
    RemoteRobotPtr remoteRobot(new RemoteRobot(robotStateComponentPrx->getSynchronizedRobot()));
    robot->setGlobalPose(remoteRobot->getGlobalPose());

    // first, we get rid of all generated grasps, whose tcp is not given in any of the preloaded workspaces
    GeneratedGraspList filteredGrasps = filterGrasps(grasps);
    visualizedGrid.reset();
    entityDrawerPrx->clearLayer(visuLayerName);


    // init collision space
    RemoteRobot::synchronizeLocalClone(robot, robotStateComponentPrx);
    cspace = new SimoxCSpaceWith2DPose(prior->getCommonStorage(), false, 30);
    AgentPlanningInformation agentData;
    agentData.agentProjectNames = robotStateComponentPrx->getArmarXPackages();
    agentData.agentRelativeFilePath = robotStateComponentPrx->getRobotFilename();
    //    agentData.kinemaicChainNames = robotNodeSetNames;
    agentData.kinemaicChainNames = {};
    agentData.collisionSetNames = {colModel};
    agentData.initialJointValues = robot->getRobotNodeSet(colModel)->getJointValueMap();
    cspace->setAgent(agentData);
    cspace->addObjectsFromWorkingMemory(wm);
    cspace->initCollisionTest();

    int placmentsPerGrasp = getProperty<int>("PlacmentsPerGrasp");
    ARMARX_VERBOSE << "Searching " << placmentsPerGrasp << " poses for " << filteredGrasps.size() << " grasps for object id " << objectInstanceEntityId;

    for (int i = 0; i < placmentsPerGrasp; ++i)
    {
        std::transform(filteredGrasps.begin(), filteredGrasps.end(), std::back_inserter(result), [&](const GeneratedGrasp & g)
        {
            FramedPosePtr newPose = new FramedPose(robot->getGlobalPose(), GlobalFrame, "");

            ObjectInstancePtr instance = ObjectInstancePtr::dynamicCast(objectInstances->getEntityById(objectInstanceEntityId));
            Eigen::Matrix4f objectPose = instance->getPose()->toGlobalEigen(robot);

            // create a workspace grid and find a suitable position
            TIMING_START(CreateWorksspaceGrid);
            VirtualRobot::WorkspaceGridPtr reachGrid = createWorkspaceGrid(g, objectPose);
            TIMING_END(CreateWorksspaceGrid);
            TIMING_START(DrawWorksspaceGrid);
            drawWorkspaceGrid(visualizedGrid);
            TIMING_END(DrawWorksspaceGrid);


            float xGoal, yGoal, platformRotation;
            getSuitablePosition(reachGrid, objectPose, xGoal, yGoal, platformRotation);

            newPose->position->x = xGoal;
            newPose->position->y = yGoal;
            Eigen::Matrix4f newPoseEigen = newPose->toGlobalEigen(robot);

            // we rotate the pose around its local z axis
            float x = newPoseEigen(0, 3);
            float y = newPoseEigen(1, 3);
            float z = newPoseEigen(2, 3);
            newPoseEigen.block<3, 1>(0, 3) = Eigen::Vector3f::Zero();
            Eigen::Affine3f transform;
            transform = Eigen::Translation<float, 3>(x, y, z) * Eigen::AngleAxisf(platformRotation, Eigen::Vector3f(0, 0, 1));
            newPoseEigen = transform * newPoseEigen;

            //        drawRobot(newPoseEigen);

            armarx::FramedPosePtr resultPose(new FramedPose(newPoseEigen, GlobalFrame, ""));
            ARMARX_INFO << "Inserting robot placement: " << resultPose->output();
            return GraspingPlacement {g, resultPose};
        });
    }


    //    TIMING_END(RobotPlacement);
    entityDrawerPrx->removeLayer(visuLayerName);
    return result;
}

GraspingPlacementList SimpleRobotPlacement::generateRobotPlacementsForGraspPose(const std::string& endEffectorName, const FramedPoseBasePtr& target, const PlanarObstacleList& planarObstacles, const ConvexHull& placementArea, const Ice::Current& c)
{
    planningTasks.clear();
    GraspingPlacementList result;

    if (!hasWorkspace(endEffectorName))
    {
        ARMARX_ERROR << "No pre-loaded workspace found for EEF '" << endEffectorName << "'";
        return result;
    }

    FramedPosePtr target_pose = FramedPosePtr::dynamicCast(target);
    target_pose->changeToGlobal(robotStateComponentPrx->getSynchronizedRobot());

    RemoteRobotPtr remoteRobot(new RemoteRobot(robotStateComponentPrx->getSynchronizedRobot()));
    robot->setGlobalPose(remoteRobot->getGlobalPose());
    RemoteRobot::synchronizeLocalClone(robot, robotStateComponentPrx);

    AgentPlanningInformation agentData;
    agentData.agentProjectNames = robotStateComponentPrx->getArmarXPackages();
    agentData.agentRelativeFilePath = robotStateComponentPrx->getRobotFilename();
    agentData.kinemaicChainNames = {};
    agentData.collisionSetNames = {colModel};
    agentData.initialJointValues = robot->getRobotNodeSet(colModel)->getJointValueMap();

    visualizedGrid.reset();
    entityDrawerPrx->clearLayer(visuLayerName);

    // Initialize Simox collision space
    cspace = new SimoxCSpaceWith2DPose(prior->getCommonStorage(), false, 30);
    cspace->setAgent(agentData);
    cspace->addObjectsFromWorkingMemory(wm);

    for (auto& obstacle : planarObstacles)
    {
        std::vector<Eigen::Vector3f> plane;

        for (auto& p : obstacle)
        {
            plane.push_back(FramedPositionPtr::dynamicCast(p)->toGlobalEigen(robotStateComponentPrx->getSynchronizedRobot()));
        }

        cspace->addPlanarObject(plane);
    }

    cspace->initCollisionTest();

    GeneratedGrasp g;
    g.score = 1;
    g.eefName = endEffectorName;
    g.framedPose = target_pose;

    int placmentsPerGrasp = getProperty<int>("PlacmentsPerGrasp");
    ARMARX_INFO << "Searching " << placmentsPerGrasp << " poses for EEF pose " << target_pose->toEigen();

    // Construct placement area as convex hull
    VirtualRobot::MathTools::ConvexHull2DPtr placementArea_ch;
    if (placementArea.size() > 2)
    {
        std::vector<Eigen::Vector2f> area;
        for (auto& p : placementArea)
        {
            area.push_back(FramedPositionPtr::dynamicCast(p)->toGlobalEigen(robot).head(2));
        }
        placementArea_ch = VirtualRobot::MathTools::createConvexHull2D(area);

        ARMARX_INFO << "Suitable placement area:";
        for (auto& p : placementArea_ch->vertices)
        {
            ARMARX_INFO << p;
        }
    }

    for (int i = 0; i < placmentsPerGrasp; ++i)
    {
        // Create Simox workspace grid
        VirtualRobot::WorkspaceGridPtr grid = createWorkspaceGrid(g, target_pose->toEigen());
        drawWorkspaceGrid(visualizedGrid);

        float xGoal, yGoal, platformRotation;
        getSuitablePosition(grid, target_pose->toEigen(), xGoal, yGoal, platformRotation, placementArea_ch);

        if (xGoal == 0 && yGoal == 0 && platformRotation == 0)
        {
            // This indicates that no suitable pose has been found
            continue;
        }

        FramedPosePtr newPose = new FramedPose(robot->getGlobalPose(), GlobalFrame, "");
        newPose->position->x = xGoal;
        newPose->position->y = yGoal;
        Eigen::Matrix4f newPoseEigen = newPose->toGlobalEigen(robot);

        // we rotate the pose around its local z axis
        float x = newPoseEigen(0, 3);
        float y = newPoseEigen(1, 3);
        float z = newPoseEigen(2, 3);
        newPoseEigen.block<3, 1>(0, 3) = Eigen::Vector3f::Zero();
        Eigen::Affine3f transform;
        transform = Eigen::Translation<float, 3>(x, y, z) * Eigen::AngleAxisf(platformRotation, Eigen::Vector3f(0, 0, 1));
        newPoseEigen = transform * newPoseEigen;

        armarx::FramedPosePtr resultPose(new FramedPose(newPoseEigen, GlobalFrame, ""));
        result.push_back(GraspingPlacement {g, resultPose});
    }

    entityDrawerPrx->removeLayer(visuLayerName);
    return result;
}

VirtualRobot::WorkspaceGridPtr SimpleRobotPlacement::createWorkspaceGrid(GeneratedGrasp g, const Eigen::Matrix4f& globalGraspPose)
{
    VirtualRobot::WorkspaceGridPtr reachGrid;
    static int counter = 0;
    std::string graspName = "some_random_grasp_" + std::to_string(counter++);
    std::string robotType = robotName;
    std::string eef = g.eefName;

    // dummy manipulation object
    VirtualRobot::ManipulationObjectPtr dummyObject(new ManipulationObject("dummyObject"));
    dummyObject->setGlobalPose(globalGraspPose);

    // dummy grasp
    Eigen::Matrix4f tcpPoseGlobal = FramedPosePtr::dynamicCast(g.framedPose)->toGlobalEigen(robot);
    Eigen::Matrix4f objectPoseInTcpFrame = tcpPoseGlobal.inverse() * globalGraspPose;
    VirtualRobot::GraspPtr dummyGrasp(new Grasp(graspName, robotType, eef, objectPoseInTcpFrame));

    WorkspaceRepresentationPtr ws;
    // find a workspace whose tcp is equal to the tcp of the generated grasp
    for (WorkspaceRepresentationPtr workspace : workspaces)
    {
        if (workspace->getTCP()->getName() == robot->getEndEffector(g.eefName)->getTcp()->getName())
        {
            ws = workspace;
            break;
        }
    }

    // create workspace grid and fill it
    Eigen::Vector3f minBB, maxBB;
    ws->getWorkspaceExtends(minBB, maxBB);
    reachGrid.reset(new WorkspaceGrid(minBB(0), maxBB(0), minBB(1), maxBB(1), ws->getDiscretizeParameterTranslation()));
    reachGrid->setGridPosition(globalGraspPose(0, 3), globalGraspPose(1, 3));
    reachGrid->fillGridData(ws, dummyObject, dummyGrasp, robot->getRootNode());

    //    if (!visualizedGrid)
    {
        visualizedGrid.reset(new WorkspaceGrid(minBB(0), maxBB(0), minBB(1), maxBB(1), ws->getDiscretizeParameterTranslation()));
        visualizedGrid->setGridPosition(globalGraspPose(0, 3), globalGraspPose(1, 3));
    }

    visualizedGrid->fillGridData(ws, dummyObject, dummyGrasp, robot->getRootNode());

    return reachGrid;
}

void SimpleRobotPlacement::getSuitablePosition(WorkspaceGridPtr reachGrid, const Eigen::Matrix4f& globalGraspPose, float& storeGlobalX, float& storeGlobalY, float& storeGlobalYaw, const VirtualRobot::MathTools::ConvexHull2DPtr& placementArea)
{
    int entry = 0;

    // robot pose
    Eigen::Matrix4f originalRobotPoseGlobal = robot->getGlobalPose();
    Eigen::Matrix4f tmpRobotPoseGlobal = originalRobotPoseGlobal;

    // workspace grid params
    float minX, maxX, minY, maxY;
    reachGrid->getExtends(minX, maxX, minY, maxY);
    int nX = 0;
    int nY = 0;
    reachGrid->getCells(nX, nY);
    int minRequiredEntry = reachGrid->getMaxEntry();

    bool collision = true;
    std::vector<GraspPtr> dummyGrasps;
    float minCollisionDistance = getProperty<float>("MinimumDistanceToEnvironment").getValue();
    int counter = 0;
    auto collisionCheckVisu = "collisionCheckRobotVisu";
    entityDrawerPrx->setRobotVisu(visuLayerName, collisionCheckVisu, robotStateComponentPrx->getRobotFilename(), boost::join(robotStateComponentPrx->getArmarXPackages(), ","), CollisionModel);
    float visuSlowdownFactor = getProperty<float>("VisualizationSlowdownFactor");

    while (collision)
    {
        counter++;
        if (counter >= 10000)
        {
            ARMARX_ERROR << "Could not find a collision free robot placement.";
            storeGlobalX = 0;
            storeGlobalY = 0;
            storeGlobalYaw = 0;
            break;
        }

        minRequiredEntry *= 0.9f;
        if (!reachGrid->getRandomPos(minRequiredEntry, storeGlobalX, storeGlobalY, dummyGrasps, 100))
        {
            continue;
        }
        entry = reachGrid->getEntry(storeGlobalX, storeGlobalY);

        if (placementArea != nullptr && !VirtualRobot::MathTools::isInside(Eigen::Vector2f(storeGlobalX, storeGlobalY), placementArea))
        {
            ARMARX_INFO << "Generated placement (" << storeGlobalX << ", " << storeGlobalY << ") lies outside the permitted area => Retry.";
            continue;
        }

        // update robot position
        tmpRobotPoseGlobal = originalRobotPoseGlobal;
        tmpRobotPoseGlobal(0, 3) = storeGlobalX;
        tmpRobotPoseGlobal(1, 3) = storeGlobalY;

        storeGlobalYaw = getPlatformRotation(tmpRobotPoseGlobal, globalGraspPose);

        // we rotate the pose around its local z axis
        float x = tmpRobotPoseGlobal(0, 3);
        float y = tmpRobotPoseGlobal(1, 3);
        float z = tmpRobotPoseGlobal(2, 3);
        if (std::isnan(x) || std::isnan(y) || std::isnan(z))
        {
            continue;
        }
        tmpRobotPoseGlobal.block<3, 1>(0, 3) = Eigen::Vector3f::Zero();
        Eigen::Affine3f transform;
        transform = Eigen::Translation<float, 3>(x, y, z) * Eigen::AngleAxisf(storeGlobalYaw, Eigen::Vector3f(0, 0, 1));
        tmpRobotPoseGlobal = transform * tmpRobotPoseGlobal;

        robot->setGlobalPose(tmpRobotPoseGlobal);
        cspace->getAgentSceneObj()->setGlobalPose(tmpRobotPoseGlobal);

        collision = cspace->getCD().isInCollision();
        updateRobot(collisionCheckVisu, tmpRobotPoseGlobal,
                    collision ? DrawColor {1.0, 0.0, 0.0, 0.5} : DrawColor {0.0, 1.0, 0.0, 0.5});
        usleep(500000 * visuSlowdownFactor);

        if (!collision)
        {
            if (minCollisionDistance > 0)
            {
                float distance = cspace->getCD().getDistance();
                if (distance < minCollisionDistance)
                {
                    collision = true;
                    continue;
                }
                ARMARX_DEBUG << "distance to objects for placement: " << distance;
            }

            if (getProperty<bool>("VisualizeCollisionSpace").getValue())
            {
                Eigen::Vector3f rpy;
                VirtualRobot::MathTools::eigen4f2rpy(tmpRobotPoseGlobal, rpy);
                armarx::VectorXf startPos {tmpRobotPoseGlobal(0, 3), tmpRobotPoseGlobal(1, 3), rpy(2)};
                MotionPlanningServerInterfacePrx mps = getProxy<MotionPlanningServerInterfacePrx>("MotionPlanningServer", false, "", false);
                if (mps)
                {
                    SimoxCSpaceWith2DPosePtr tmpCSpace = SimoxCSpaceWith2DPosePtr::dynamicCast(cspace->clone());
                    auto agent = tmpCSpace->getAgent();
                    agent.agentPose = new Pose(tmpRobotPoseGlobal);
                    tmpCSpace->setAgent(agent);

                    CSpaceVisualizerTaskHandle taskHandle = mps->enqueueTask(new CSpaceVisualizerTask(tmpCSpace, startPos, getDefaultName() + "Visu" + IceUtil::generateUUID()));
                    planningTasks.push_back(taskHandle);
                }
            }
        }
    }
    //entityDrawerPrx->removeRobotVisu(visuLayerName, collisionCheckVisu);
    robot->setGlobalPose(originalRobotPoseGlobal);
}

float SimpleRobotPlacement::getPlatformRotation(const Eigen::Matrix4f& frameGlobal, const Eigen::Matrix4f& globalTarget)
{
    Eigen::Matrix4f localTarget = frameGlobal.inverse() * globalTarget;
    float x = localTarget(0, 3);
    float y = localTarget(1, 3);

    float alpha = std::atan2(y, x);
    alpha -= M_PI / 2; // armars face direction is the positive y-axis, therefore -pi/2
    return alpha;
}

void SimpleRobotPlacement::loadRobot()
{
    robot = VirtualRobot::RobotIO::loadRobot(robotFilePath);
    if (!robot)
    {
        ARMARX_ERROR << "Failed to load robot: " << robotFilePath;
        return;
    }

    RemoteRobotPtr remoteRobot(new RemoteRobot(robotStateComponentPrx->getSynchronizedRobot()));
    robot->setGlobalPose(remoteRobot->getGlobalPose());
}

void SimpleRobotPlacement::loadWorkspaces()
{
    for (std::string wsFile : workspaceFilePaths)
    {
        WorkspaceRepresentationPtr newSpace;
        bool success = false;

        // 1st try to load as manipulability file
        try
        {
            newSpace.reset(new Manipulability(robot));
            newSpace->load(wsFile);
            success = true;

            ARMARX_INFO << "Map '" << wsFile << "' loaded as Manipulability map";
        }
        catch (...)
        {
        }

        // 2nd try to load as reachability file
        if (!success)
        {
            try
            {
                newSpace.reset(new Reachability(robot));
                newSpace->load(wsFile);
                success = true;

                ARMARX_INFO << "Map '" << wsFile << "' loaded as Reachability map";
            }
            catch (...)
            {
                armarx::handleExceptions();
            }
        }

        if (success)
        {
            workspaces.push_back(newSpace);
        }
        else
        {
            ARMARX_ERROR << "Failed to load map '" << wsFile << "'";
        }
    }
}

bool SimpleRobotPlacement::hasWorkspace(std::string tcp)
{
    for (WorkspaceRepresentationPtr ws : workspaces)
    {
        if (ws->getTCP()->getName() == tcp)
        {
            return true;
        }
    }
    return false;
}

GeneratedGraspList SimpleRobotPlacement::filterGrasps(const GeneratedGraspList grasps)
{
    GeneratedGraspList filteredGrasps = grasps;
    GeneratedGraspList::iterator it = filteredGrasps.begin();
    while (it != filteredGrasps.end())
    {
        GeneratedGrasp g = (*it);
        auto tcpName = robot->getEndEffector(g.eefName)->getTcp()->getName();
        if (!hasWorkspace(tcpName))
        {
            ARMARX_VERBOSE << "Removing grasp because tcp " << tcpName << " is not available in workspace";
            it = filteredGrasps.erase(it);
        }
        else
        {
            ++it;
        }
    }
    return filteredGrasps;
}

void SimpleRobotPlacement::drawNewRobot(Eigen::Matrix4f globalPose)
{
    static int suffix =  0;
    std::string id = drawRobotId + std::to_string(suffix++);
    entityDrawerPrx->setRobotVisu(visuLayerName, id, robotStateComponentPrx->getRobotFilename(), boost::join(robotStateComponentPrx->getArmarXPackages(), ","), CollisionModel);
    updateRobot(id, globalPose, COLOR_ROBOT);
}

void SimpleRobotPlacement::updateRobot(std::string id, Eigen::Matrix4f globalPose, DrawColor color)
{
    entityDrawerPrx->updateRobotColor(visuLayerName, id, color);
    entityDrawerPrx->updateRobotPose(visuLayerName, id, new Pose(globalPose));
}

void SimpleRobotPlacement::drawWorkspaceGrid(const GeneratedGrasp& grasp, const std::string& objectInstanceEntityId)
{
    ObjectInstancePtr instance = ObjectInstancePtr::dynamicCast(objectInstances->getEntityById(objectInstanceEntityId));
    Eigen::Matrix4f objectPose = instance->getPose()->toGlobalEigen(robot);

    drawWorkspaceGrid(createWorkspaceGrid(grasp, objectPose));
}

void SimpleRobotPlacement::drawWorkspaceGrid(WorkspaceGridPtr reachGrid)
{
    int counter = 0;
    Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
    float minX, maxX, minY, maxY;
    reachGrid->getExtends(minX, maxX, minY, maxY);
    pose(0, 3) = minX;
    pose(1, 3) = minY;

    int nX = 0;
    int nY = 0;
    reachGrid->getCells(nX, nY);

    int maxEntry = reachGrid->getMaxEntry();

    float sizeX = (maxX - minX) / (float)nX;
    float sizeY = (maxY - minY) / (float)nY;
    auto batch = debugDrawerPrx->ice_batchOneway();
    // iterate through the workspace grid
    for (int x = 0; x < nX; x++)
    {
        float xPos = minX + (float)x * sizeX + 0.5f * sizeX; // x-center of voxel

        for (int y = 0; y < nY; y++)
        {
            int cellEntry = reachGrid->getCellEntry(x, y);

            if (cellEntry > 0)
            {
                float intensity = (float)cellEntry / (float)maxEntry;

                float yPos = minY + (float)y * sizeY + 0.5f * sizeY; // y-center of voxel
                pose(0, 3) = xPos;
                pose(1, 3) = yPos;

                armarx::Vector3Ptr dimensions = new armarx::Vector3(30, 30, 1);
                armarx::PosePtr tmpPose = new armarx::Pose(pose);

                VirtualRobot::ColorMap cm = VirtualRobot::ColorMap::eHot;
                VirtualRobot::CoinVisualizationFactory::Color color = cm.getColor(intensity);

                armarx::DrawColor voxelColor;
                voxelColor.r = color.r;
                voxelColor.g = color.g;
                voxelColor.b = color.b;
                voxelColor.a = 0.5;

                batch->setBoxVisu(visuLayerName, "reachGridVoxel_" + std::to_string(counter++),
                                  tmpPose, dimensions, voxelColor);
            }
        }
    }
    batch->ice_flushBatchRequests();
}
