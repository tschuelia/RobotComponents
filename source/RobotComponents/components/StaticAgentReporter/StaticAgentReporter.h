/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents::ArmarXObjects::StaticAgentReporter
 * @author     Timothee Habra ( timothee dot habra at uclouvain dot be )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotComponents_StaticAgentReporter_H
#define _ARMARX_COMPONENT_RobotComponents_StaticAgentReporter_H


#include <ArmarXCore/core/Component.h>

#include <MemoryX/interface/component/WorkingMemoryInterface.h>
#include <MemoryX/libraries/memorytypes/entity/AgentInstance.h>

namespace armarx
{
    /**
     * @class StaticAgentReporterPropertyDefinitions
     * @brief
     */
    class StaticAgentReporterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        StaticAgentReporterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of the WorkingMemory that should be used. Leave empty to not use the working memory and update the robotstate directly");

        }
    };

    /**
     * @defgroup Component-StaticAgentReporter StaticAgentReporter
     * @ingroup RobotComponents-Components
     * A description of the component StaticAgentReporter.
     * 
     * @class StaticAgentReporter
     * @ingroup Component-StaticAgentReporter
     * @brief Brief description of class StaticAgentReporter.
     * 
     * Detailed description of class StaticAgentReporter.
     */
    class StaticAgentReporter :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "StaticAgentReporter";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:


        RobotStateComponentInterfacePrx robotStateComponent;

        memoryx::AgentInstancesSegmentBasePrx agentInstanceSegment;
        memoryx::AgentInstancePtr robotAgent;

        VirtualRobot::RobotPtr robot;
    };
}

#endif
