/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author Fabian Paus (fabian dot paus at kit dot edu)
 * @date 2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef QPAINTER_WIDGET_H
#define QPAINTER_WIDGET_H

#include <QWidget>
#include <QPainter>
#include <QLayout>
#include <QMouseEvent>

#include <functional>

namespace armarx
{
    typedef std::function<void(QPainter& painter)> PaintCallback;

    class QPainterWidget : public QWidget
    {
        Q_OBJECT

    signals:
        void mousePressed(QPoint p);

    public:
        void setupUi(QWidget* parent = 0)
        {
            parent->layout()->addWidget(this);

            if (objectName().isEmpty())
            {
                setObjectName(QString::fromUtf8("QPainterWidget"));
            }
            resize(372, 318);
            QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            sizePolicy.setHorizontalStretch(0);
            sizePolicy.setVerticalStretch(0);
            sizePolicy.setHeightForWidth(this->sizePolicy().hasHeightForWidth());
            setSizePolicy(sizePolicy);

            QMetaObject::connectSlotsByName(this);
        }

        void paintEvent(QPaintEvent*) override
        {
            if (callback)
            {
                QPainter painter(this);
                callback(painter);
            }
        }

        void mousePressEvent(QMouseEvent* event)
        {
            emit mousePressed(event->pos());
        }

        void setPaintCallback(PaintCallback const& callback)
        {
            this->callback = callback;
        }

    private:
        PaintCallback callback;
    };
}

#endif // QPAINTER_WIDGET_H
