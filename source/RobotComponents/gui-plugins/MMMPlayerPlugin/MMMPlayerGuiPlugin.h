/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ObjectExaminerGuiPlugin
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License

*/

#ifndef _ARMARXGUI_PLUGINS_MMMPlayerWIDGET_H
#define _ARMARXGUI_PLUGINS_MMMPlayerWIDGET_H

/* ArmarX headers */
#include "ui_MMMPlayerGuiPlugin.h"

#include "MMMPlayerConfigDialog.h"
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

/* Qt headers */
#include <QtGui/QMainWindow>
#include <QtCore/QTimer>
#include <RobotComponents/interface/components/MMMPlayerInterface.h>


#include <RobotComponents/interface/components/TrajectoryPlayerInterface.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <string>
#include <QLayout>
#include <QFileDialog>
#include <QListWidget>



namespace armarx
{
    class MMMPlayerConfigDialog;

    /*!
      \class MMMPlayerGuiPlugin
      \brief This plugin provides a widget with which the MMMPlayer can be controlled.

      \see MMMPlayerWidget
      */
    class MMMPlayerGuiPlugin :
        public ArmarXGuiPlugin
    {
    public:
        MMMPlayerGuiPlugin();
        QString getPluginName()
        {
            return "MMMPlayerGuiPlugin";
        }
    };

    /*!
      \class MMMPlayerWidget
      \brief With this widget the MMMPlayer can be controlled.

      \ingroup RobotAPI-ArmarXGuiPlugins ArmarXGuiPlugins
      \see MMMPlayerGuiPlugin
      */
    class MMMPlayerWidget :
        public ArmarXComponentWidgetControllerTemplate<MMMPlayerWidget>
    {
        Q_OBJECT
    public:
        MMMPlayerWidget();
        ~MMMPlayerWidget()
        {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();
        virtual void onDisconnectComponent();

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.MMMPlayerGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/MMM.png");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0)
        {
            if (!dialog)
            {
                dialog = new MMMPlayerConfigDialog(parent);
            }

            return qobject_cast<MMMPlayerConfigDialog*>(dialog);

        }

        bool onClose();



        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        //        void configured();

        void startComponent();
        void loadTrajPlayer();
        void setSpeed(double value);
    signals:

    private slots:

        void on_initializeButton_clicked();

        void on_startButton_clicked();

        void on_previewButton_clicked();

        void on_pauseButton_clicked();

        void on_stopButton_clicked();

        void configFileSelected();

        void updateSlider();

        void setupJointList();

        void on_loopPlayback_toggled(bool state);

        void jointListChanged(QListWidgetItem* joint);

        void on_spinBoxFPS_valueChanged(double value);

        void on_controlMode_changed(int controlMode);

        void on_enableRobotPoseUnit_toggled(bool state);

    protected:
        void connectSlots();

        std::string configFile;
        Ui::MMMPlayerGuiPlugin ui;
        MMMPlayerInterfacePrx MMMLoader;
        TrajectoryPlayerInterfacePrx trajPlayer;
    private:
        QPointer<QWidget> __widget;
        QPointer<MMMPlayerConfigDialog> dialog;
        QFileDialog* fileDialog2;
        QListWidget* jointList;

        QTimer* updateTimer;
        bool isComponentCreated;
    };
    //typedef boost::shared_ptr<MMMPlayerWidget> MMMPlayerGuiPluginPtr;
}

#endif
