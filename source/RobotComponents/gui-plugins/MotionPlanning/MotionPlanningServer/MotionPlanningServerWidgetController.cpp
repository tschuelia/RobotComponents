/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <string>

#include <QPushButton>
#include <QTableWidget>

#include <RobotComponents/components/MotionPlanning/util/PlanningUtil.h>

#include "MotionPlanningServerWidgetController.h"

using namespace armarx;

MotionPlanningServerWidgetController::MotionPlanningServerWidgetController():
    motionPlanningServerProxyName {"MotionPlanningServer"}
{
    widget.setupUi(getWidget());
    taskList = new MotionPlanningServerTaskList{};
    widget.verticalLayout->addWidget(taskList);
}

void MotionPlanningServerWidgetController::loadSettings(QSettings* settings)
{
    motionPlanningServerProxyName = settings->value("motionPlanningServerProxyName", QString::fromStdString(motionPlanningServerProxyName)).toString().toStdString();
}

void MotionPlanningServerWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("motionPlanningServerProxyName", QString::fromStdString(motionPlanningServerProxyName));
}

void MotionPlanningServerWidgetController::onInitComponent()
{
    usingProxy(motionPlanningServerProxyName);
}

void MotionPlanningServerWidgetController::onConnectComponent()
{
    taskList->setMotionPlanningServer(getProxy<MotionPlanningServerInterfacePrx>(motionPlanningServerProxyName));
    taskList->enableAutoUpdate(true);
}

void MotionPlanningServerWidgetController::onDisconnectComponent()
{
    taskList->clearList();
    taskList->setMotionPlanningServer(nullptr);
    taskList->enableAutoUpdate(false);
}

QPointer<QDialog> MotionPlanningServerWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new MotionPlanningServerConfigDialog(parent);
    }

    return qobject_cast<MotionPlanningServerConfigDialog*>(dialog);
}

void MotionPlanningServerWidgetController::configured()
{
    motionPlanningServerProxyName = dialog->finder->getSelectedProxyName().toStdString();
}

