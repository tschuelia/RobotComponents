/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_GUIPLUGIN_RobotComponents_PlanningServer_WidgetController_H
#define _ARMARX_GUIPLUGIN_RobotComponents_PlanningServer_WidgetController_H

#include "MotionPlanningServerConfigDialog.h"
#include "ui_MotionPlanningServerWidget.h"

#include "../QtWidgets/MotionPlanningServerTaskList.h"

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>

#include <QPointer>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{
    class MotionPlanningServerConfigDialog;

    /**
     * @class MotionPlanningServerWidgetController
     * @ingroup ArmarXGuiPlugins
     * @page RobotComponents-GuiPlugins-MotionPlanningServerWidget The plugin to show all tasks on a MotionPlanningServer
     * @brief A gui plugin to see all tasks on a MotionPlanningServer and their status.
     *
     * Per task it shows:
     * \li The ice identity
     * \li The status
     * \li The ice type
     * \li The internal id used by the \ref armarx::MotionPlanningServer. (this can be used for debugging) Tasks are processed in ascending order.
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        MotionPlanningServerWidgetController:
        public ArmarXComponentWidgetControllerTemplate<MotionPlanningServerWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit MotionPlanningServerWidgetController();

        /**
         * Controller destructor
         */
        virtual ~MotionPlanningServerWidgetController() = default;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         * @return The Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "MotionPlanning.MotionPlanningServerGui";
        }

        /**
         * @brief Returns the plugin's config dialog.
         * @param parent The dialog's parent.
         * @return The plugin's config dialog.
         */
        virtual QPointer<QDialog> getConfigDialog(QWidget* parent = nullptr) override;
        /**
         * @brief Callback called when the config dialog is closed.
         */
        virtual void configured() override;

        /**
         * @see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent() override;

        /**
         * @see armarx::Component::onConnectComponent()
         */
        virtual void onDisconnectComponent() override;

    private:
        /**
         * Widget Form
         */
        Ui::MotionPlanningServerWidget widget;

        /**
         * @brief The plugin's dialog.
         */
        QPointer<MotionPlanningServerConfigDialog> dialog;

        /**
         * @brief The planning server's name.
         */
        std::string motionPlanningServerProxyName;

        QPointer<MotionPlanningServerTaskList> taskList;
    };
}
#endif
