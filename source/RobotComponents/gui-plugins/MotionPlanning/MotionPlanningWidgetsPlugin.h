/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef ARMARX_COMPONENT_MDIGUIPLUGIN_H
#define ARMARX_COMPONENT_MDIGUIPLUGIN_H

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace armarx
{
    /**
     * @brief The MotionPlanningPlugin offers the widgets SimoxCSpaceVisualizer and MotionPlanningServer.
     *
     * @see SimoxCSpaceVisualizerWidgetController
     * @see MotionPlanningServerWidgetController
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT MotionPlanningPlugin :
        public ArmarXGuiPlugin
    {
    public:
        MotionPlanningPlugin();
    };
}

#endif
