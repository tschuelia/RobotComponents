/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef INDEXEDQCHECKBOX_H
#define INDEXEDQCHECKBOX_H

#include <QCheckBox>

namespace armarx
{
    /**
     * @brief A check box with an index. The index and check state is emitted when its check state changes.
     */
    class IndexedQCheckBox : public QCheckBox
    {
        Q_OBJECT
    public:
        /**
         * @brief ctor.
         * @param index The index to emit,
         * @param parent The widget's parent.
         */
        IndexedQCheckBox(int index, QWidget* parent = nullptr):
            QCheckBox {parent},
        index {index}
        {
            connect(this, SIGNAL(stateChanged(int)), this, SLOT(emitStateChangedWithIndex(int)));
        }
        /**
         * @brief ctor.
         * @param index The index to emit,
         * @param The checkboxes' label.
         * @param parent The widget's parent.
         */
        IndexedQCheckBox(int index, const QString& text, QWidget* parent = nullptr):
            QCheckBox {text, parent},
        index {index}
        {
            connect(this, SIGNAL(stateChanged(int)), this, SLOT(emitStateChangedWithIndex(int)));
        }

    signals:
        /**
         * @brief Emitted when the check state changes.
         * @param index The index.
         * @param state The check state.
         */
        void stateChangedIndex(int index, Qt::CheckState state);
    private slots:
        /**
         * @brief Emitts the slot stateChangedIndex.
         * @param state The check state.
         */
        void emitStateChangedWithIndex(int state)
        {
            emit stateChangedIndex(index, static_cast<Qt::CheckState>(state));
        }

    private:
        /**
         * @brief The index to emit.
         */
        int index;
    };
}
#endif
