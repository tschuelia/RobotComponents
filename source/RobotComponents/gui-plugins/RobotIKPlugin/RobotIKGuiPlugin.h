/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Philipp Schmidt
* @date       2015
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef RobotIKGUIPLUGIN_H
#define RobotIKGUIPLUGIN_H

//Gui
#include "ui_RobotIKGuiPlugin.h"

//Visualization
#include "ManipulatorVisualization.h"

//ArmarX includes
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <RobotComponents/interface/components/RobotIK.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

//Qt includes
#include <QObject>

//VirtualRobot includes
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

//Inventor includes
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/manips/SoTransformerManip.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoMaterial.h>


namespace armarx
{

    class RobotIKGuiPlugin : public ArmarXGuiPlugin
    {
    public:
        RobotIKGuiPlugin();

        QString getPluginName()
        {
            return "RobotIKGuiPlugin";
        }
    };

    /**
     * \class RobotIKWidget
     * \page RobotComponents-GuiPlugins-RobotIK "The RobotIK Gui Plugin"
     *
     * \brief The RobotIK Gui Plugin can be used for IK computation.
     *
     * This gui plugin can be used for Cartesian control of a robot (arm). It connects to the KinemticUnit of the robot
     * in order to execute the computed configurations via position control.
     * Via the plugin the kinematic chain of the robot can be specified allowing to move different end effectors with varying subsets of the robot's joints.
     *
     *
     * \image html RobotIKGui_small.png "The RobotIK Gui Plugin is used to move the right end effector of Armar-III."
     *
     * Demonstration video of the RobotIK Gui:
     * \htmlonly
     * <p align="middle">
     * <iframe align="center" width="560" height="315" src="https://www.youtube.com/embed/jyPdg5QlcH4?rel=0?ecver=1" frameborder="0" allowfullscreen></iframe>
     * </p>
     * \endhtmlonly
     *
     *
     */
    //     * \ingroup RobotAPI-ArmarXGuiPlugins ArmarXGuiPlugins

    class RobotIKWidgetController : public ArmarXComponentWidgetControllerTemplate<RobotIKWidgetController>
    {
        Q_OBJECT

    public:
        RobotIKWidgetController();
        virtual ~RobotIKWidgetController() {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.RobotIK";
        }
        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

    public slots:
        void initWidget();
        void solveIK();
        void kinematicChainChanged(const QString& arg1);
        void caertesianSelectionChanged(const QString& arg1);
        void autoFollowChanged(bool checked);
        void resetManip();
        void updateSolutionDisplay();

    protected:
        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;
        RobotIKInterfacePrx robotIKPrx;

        Ui::RobotIKGuiPlugin ui;

    private slots:
        void on_btnCopyCurrentPoseToClipboard_clicked();
        void onDisconnect();

    private:
        void ikCallbackExecuteMotion(const Ice::AsyncResultPtr& r);
        void ikCallbackDisplaySolution(const Ice::AsyncResultPtr& r);
        void connectSlots();
        std::string robotStateComponentName;
        std::string kinematicUnitComponentName;
        std::string robotIKComponentName;

        QPointer<SimpleConfigDialog> dialog;

        VirtualRobot::RobotPtr robot;
        Ice::StringSeq getIncludePaths();
        VirtualRobot::RobotPtr loadRobot(Ice::StringSeq includePaths);

        ManipulatorVisualization* visualization;

        static void manipFinishCallback(void* data, SoDragger* manip);
        static void manipMovedCallback(void* data, SoDragger* manip);

        SoTimerSensor* robotUpdateSensor;
        static void robotUpdateTimerCB(void* data, SoSensor* sensor);

        SoTimerSensor* textFieldUpdateSensor;
        static void textFieldUpdateTimerCB(void* data, SoSensor* sensor);

        SoTimerSensor* autoFollowSensor;
        static void autoFollowSensorTimerCB(void* data, SoSensor* sensor);
        bool manipulatorMoved;

        ExtendedIKResult getIKSolution();

        CartesianSelection convertOption(std::string option);

        bool startUpCameraPositioningFlag;
        Mutex solutionMutex;
        armarx::ExtendedIKResult currentSolution;

    };
    typedef boost::shared_ptr<RobotIKWidgetController> RobotIKGuiPluginPtr;
    typedef boost::shared_ptr<VirtualRobot::CoinVisualization> CoinVisualizationPtr;
}

#endif
