/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    
 * @author     
 * @date       
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RobotViewerWidget.h"

#include <QGridLayout>

armarx::RobotViewerWidget::RobotViewerWidget(QWidget* parent) : QWidget(parent)
{
    this->setContentsMargins(1, 1, 1, 1);

    QGridLayout* grid = new QGridLayout();
    grid->setContentsMargins(0, 0, 0, 0);
    this->setLayout(grid);
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    QWidget* view1 = new QWidget(this);
    view1->setMinimumSize(100, 100);
    view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    viewer.reset(new RobotViewer(view1));
    viewer->show();

    grid->addWidget(view1, 0, 0, 1, 2);
}


armarx::RobotViewerWidget::~RobotViewerWidget()
{

}

RobotViewerPtr armarx::RobotViewerWidget::getRobotViewer()
{
    return viewer;
}
