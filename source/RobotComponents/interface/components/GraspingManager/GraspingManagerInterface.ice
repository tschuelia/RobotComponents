/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     Valerij Wittenbeck (valerij dot wittenbeck at student dot kit dot edu)
* @copyright  2016 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ROBOTCOMPONENTS_COMPONENT_GraspingManager_SLICE_
#define _ROBOTCOMPONENTS_COMPONENT_GraspingManager_SLICE_

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/core/Trajectory.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
//    struct RobotConfigTrajectoryPoint
//    {
//        float timeStamp;
//        NameValueMap jointConfig;
//    };

//    sequence<RobotConfigTrajectoryPoint> RobotConfigTrajectory;

//    struct RobotPoseTrajectoryPoint
//    {
//        float timeStamp;
//        FramedPoseBase robotPose;
//    };

//    sequence<RobotPoseTrajectoryPoint> RobotPoseTrajectory;

    struct GraspingTrajectory
    {
        TrajectoryBase poseTrajectory;
        TrajectoryBase configTrajectory;
        string rnsToUse;
        string endeffector;
    };

    sequence<GraspingTrajectory> GraspingTrajectoryList;

    interface GraspingManagerInterface
    {
        GraspingTrajectory generateGraspingTrajectory(string objectInstanceEntityId);
    };

};

#endif



