/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_INTERFACE_RobotComponents_PlanningServer_DataStructures_SLICE
#define _ARMARX_INTERFACE_RobotComponents_PlanningServer_DataStructures_SLICE

#include <RobotAPI/interface/core/PoseBase.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module armarx
{
    struct Path
    {
        VectorXfSeq nodes;
        string pathName = "";
    };

    sequence<Path> PathSeq;

    /**
     * @brief Struct containing a path and its cost.
     */
    struct PathWithCost
    {
        /**
         * @brief The path.
         */
        VectorXfSeq nodes;
        /**
         * @brief The path's cost.
         */
        float cost;
        string pathName = "";
    };

    sequence<PathWithCost> PathWithCostSeq;

    /**
     * @brief We want to stop the status enum from polluting the surrounding namespace
     * This simulates enum class from c++11
     */
    module TaskStatus
    {

    /**
     * @brief The task's execution status.
     *
     *  State transitions ({from1,from2} =(event)> target):
     *      - =(creation)> eQueued
     *      - eQueued =(dispatched for execution)> ePlanning
     *      - ePlanning =(found path)> eRefining
     *      - eRefining =(finished refinement)> eDone
     *      - {eQueued, ePlanning} =(abortTask())> ePlanningAborted
     *      - eRefining =(abortTask())> eRefinementAborted
     *      - ePlanning =(no path found)> ePlanningFailed
     *
     * If a new status is added the functions in
     * PlanningStatus.cpp
     * have to be changed (else some will assert(false)).
     *
     */
        enum Status
        {
            ///@brief The task was not added to any planning server
            eNew,
            ///@brief Task is waiting for execution
            eQueued,
            ///@brief Task is running and n opath was found jet.
            ePlanning,
            ///@brief A path was found and now the solution is improved.
            eRefining,
            ///@brief Task was aborted before a path was found.
            ePlanningAborted,
            ///@brief Path planning failed and no path was found.
            ePlanningFailed,
            ///@brief The refinement was aborted. (A valid solution does exist)
            eRefinementAborted,
            ///@brief Task refinement was completed.
            eDone,
            ///@brief Task crashed.
            eException,
            eTaskStatusSize
        };
    };

    /**
     * @brief Structure containing task id, status and ice type.
     */
    struct TaskInfo
    {
            /**
             * @brief The tasks internal id.
             */
            long internalId;
            /**
             * @brief The task identity.
             */
            Ice::Identity taskIdent;
            /**
             * @brief The task status.
             */
            TaskStatus::Status status;
            /**
             * @brief The ice type id.
             */
            string typeId;
            string taskName;
    };

    sequence<TaskInfo> TaskInfoSeq;
};
#endif
