/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_AdaptiveDynamicDomainInformedRRTStar_Task_SLICE
#define _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_AdaptiveDynamicDomainInformedRRTStar_Task_SLICE

#include <RobotComponents/interface/components/MotionPlanning/Tasks/CPRSAwareMotionPlanningTask.ice>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/AdaptiveDynamicDomainInformedRRTStar/DataStructures.ice>

module armarx
{
module addirrtstar
{
    interface ResourceManagementInterface {
        /**
          * @brief
          */
        idempotent void setMaxCpus(int maxCpus);

        ["cpp:const"] idempotent int getMaxCpus();
    };

    /**
     * @brief The task containing information for a rrt* using adaptive dynamic domain with informed sampling
     */
    ["cpp:virtual"]
    class TaskBase extends cprs::CPRSAwareMotionPlanningTaskBase implements MotionPlanningMultiPathWithCostTaskControlInterface, ResourceManagementInterface
    {
        /**
         * @brief Stores the paths in the task.
         * Used by the manager node to store the results before shut down.
         * @param paths The paths.
         */
        void setPaths(PathWithCostSeq paths);

        /**
         * @brief Returns the node count.
         * @return The node count.
         */
        ["cpp:const"] idempotent long getNodeCount();

        //management
        /**
         * @brief The initial worker count used when solving this task.
         */
        long initialWorkerCount;
        /**
         * @brief The maximal worker count used when solving this task.
         */
        long maximalWorkerCount;

        // //problem specific params
        /**
         * @brief The parameters required for adaptive dynamic domain.
         */
        AdaptiveDynamicDomainParameters addParams;

        /**
         * @brief The target path cost.
         */
        float targetCost;

        /**
         * @brief The size of one batch.
         */
        long batchSize;

        /**
         * @brief Number of nodes created (by a worker) before a connect to the goal node is tried (by this worker).
         */
        long nodeCountDeltaForGoalConnectionTries;
    };
};
};
#endif
