/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_MotionPlanningTask_SLICE
#define _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_MotionPlanningTask_SLICE

#include <ArmarXCore/interface/core/RemoteObjectNode.ice>

#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.ice>

#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>

module armarx
{
    interface MotionPlanningServerInterface;
    /**
     * @brief The interface used by a client to control a server side task.
     */
    interface MotionPlanningTaskControlInterface
    {
        /**
         * @brief Aborts the task.
         * Frees all allocated processing Resources and deques the task.
         * Causes a state transition.
         * @see TaskStatus::Status
         */
        idempotent void abortTask();

        /**
         * @return The found path. If no path was found an empty Path is returned.
         */
        ["cpp:const"] idempotent Path getPath();
        /**
         * @return The task's status.
         */
        ["cpp:const"] idempotent TaskStatus::Status getTaskStatus();
        /**
         * @return Whether the task is currently running.
         */
        ["cpp:const"] idempotent bool isRunning();
        /**
         * @return Whether the task has finished running.
         */
        ["cpp:const"] idempotent bool finishedRunning();

        ["amd"] void waitForFinishedPlanning();

        ["amd"] void waitForFinishedRunning();

        /**
         * @return The planning time in microseconds
         */
        ["cpp:const"] idempotent long getPlanningTime();
        /**
         * @return The refining time in microseconds
         */
        ["cpp:const"] idempotent long getRefiningTime();
        /**
         * @return The running time in microseconds
         */
        ["cpp:const"] idempotent long getRunningTime();
    };

    interface MotionPlanningWithCostTaskControlInterface extends MotionPlanningTaskControlInterface
    {
        ["cpp:const"] PathWithCost getPathWithCost();
    };

    interface MotionPlanningMultiPathTaskControlInterface extends MotionPlanningTaskControlInterface
    {
        /**
         * @brief Returns the number of found paths.
         * @return The number of found paths.
         */
        ["cpp:const"] idempotent long getPathCount();

        /**
         * @brief Returns the nth found path.
         * @return The nth found path.
         * @throw IndexOutOfBoundsException If n is not in [0, getPathCount())
         */
        ["cpp:const"] idempotent Path getNthPath(long n) throws armarx::IndexOutOfBoundsException;

        /**
         * @brief Returns all found paths.
         * @return All found paths.
         */
        ["cpp:const"] idempotent PathSeq getAllPaths();
    };

    interface MotionPlanningMultiPathWithCostTaskControlInterface extends MotionPlanningWithCostTaskControlInterface, MotionPlanningMultiPathTaskControlInterface
    {
        /**
         * @brief Returns the best paths cost. (inf if no path was found)
         * @return The best paths cost. (inf if no path was found)
         */
        ["cpp:const"] idempotent PathWithCost getBestPath();

        /**
         * @brief Returns the nth found path.
         * @return The nth found path.
         * @throw IndexOutOfBoundsException If n is not in [0, getPathCount())
         */
        ["cpp:const"] idempotent PathWithCost getNthPathWithCost(long n) throws armarx::IndexOutOfBoundsException;

        /**
         * @brief Returns all found paths.
         * @return All found paths.
         */
        ["cpp:const"] idempotent PathWithCostSeq getAllPathsWithCost();
    };

    /**
     * @brief Abstract base holding the data required to execute a planning task.
     * The algorithm is specified in derived classes.
     *
     * The c++ side requires a function to create a server side task.
     * (since this is an implementation detail this function is not part of the slice interface)
     */
    ["cpp:virtual"]
    class MotionPlanningTaskBase implements MotionPlanningTaskControlInterface
    {
        /**
         * @brief Runs the task and blocks until it is finished (done/arborted/failed)
         *
         * The function is protected, since it should only be called locally on the server side.
         * @param remoteObjectNodes The remote object nodes to use for calculation
         */
        ["protected"] void run(armarx::RemoteObjectNodePrxList remoteObjectNodes);

        ["protected"] bool setTaskStatus(TaskStatus::Status newTaskStatus);

        /**
         * @return The CSpace.
         */
        ["cpp:const"] idempotent CSpaceBase getCSpace();

        /**
         * @return The goal config.
         */
        ["cpp:const"] idempotent long getMaximalPlanningTimeInSeconds();

        ["cpp:const"] idempotent string getTaskName();
        ["protected"] string taskName;
    };

    ["cpp:virtual"]
    class MotionPlanningTaskWithDefaultMembersBase extends MotionPlanningTaskBase
    {
        /**
         * @brief The start configuration.
         */
        ["protected"] VectorXf startCfg;

        /**
         * @brief The goal configuration.
         */
        ["protected"] VectorXf goalCfg;

        //no end vector since some algorithm could use a different criteria than an end point. (e.g. a goal region, grasp)
        /**
         * @brief The used planning cspace.
         */
        ["protected"] CSpaceBase cspace;
        /**
         * @brief The step size used for dcd.
         */
        ["protected"] float dcdStep = 0.01;
        /**
         * @brief The maximal allowed planning time in seconds.
         * (if this limit is reached the task is aborted).
         */
        ["protected"] long maximalPlanningTimeInSeconds = 600;

        /**
         * @return The start configuration.
         */
        ["cpp:const"] idempotent VectorXf getStart();

        /**
         * @return The goal config.
         */
        ["cpp:const"] idempotent VectorXf getGoal();

        /**
         * @return The step size used for dcd.
         */
        ["cpp:const"] idempotent float getDcdStep();
    };

    ["cpp:virtual"]
    class PostprocessingMotionPlanningTaskBase extends MotionPlanningTaskBase
    {
        MotionPlanningTaskBase previousStep;
    };
};
#endif
