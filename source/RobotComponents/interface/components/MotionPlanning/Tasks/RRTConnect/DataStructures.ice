/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_RRTConnect_DataStructures_SLICE
#define _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_RRTConnect_DataStructures_SLICE

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotComponents/interface/components/MotionPlanning/DataStructures.ice>

module armarx
{
module rrtconnect
{
    /**
     * @brief Structure containing the worker's id that created the node and the number of nodes created by this worker.
     */
    struct NodeId
    {
        /**
         * @brief The worker's id that created the node.
         */
        long workerId;
        /**
         * @brief The node's id for the given worker and tree.
         */
        long nodeSubId;
    };
    sequence<NodeId> NodeIdList;

    /**
     * @brief Update for the creation of a node.
     */
    struct NodeCreationUpdate
    {
        /**
         * @brief The node's config.
         */
        VectorXf config;
        /**
         * @brief The node's parent.
         */
        NodeId parent;
    };
    sequence<NodeCreationUpdate> NodeCreationUpdateList;

    struct PerTreeUpdate
    {
        //the node ids for all new nodes are implicit created by the position in the list in combination with the workerId and oldNodeCounts[workerId]
        /**
         * @brief Node creation updates.
         */
        NodeCreationUpdateList nodes;
    };
    sequence<PerTreeUpdate> PerTreeUpdateList;


    /**
     * @brief Compound update structure containing all updates.
     */
    struct Update
    {
        /**
         * @brief The worker's id causing the update.
         */
        long workerId;

        /**
         * @brief The updates of workers prior to this update.
         * These ids are used to apply updares in correct order, detect missing updates and request missing updates again.
         * dependetOnUpdateIds[i] == -1 means no updates from worker i are required
         */
        Ice::LongSeq dependetOnUpdateIds;

        PerTreeUpdateList updatesPerTree;
    };
    sequence<Update> UpdateList;

    /**
     * @brief Interface used to transmit tree updates.
     */
    interface TreeUpdateInterface
    {
        /**
         * @brief Queues the update for integration into the tree
         * @param u The update data
         */
        void updateTree(Update u);

        /**
         * @brief Stopps planning.
         */
        void abort();
    };
};
};
#endif
