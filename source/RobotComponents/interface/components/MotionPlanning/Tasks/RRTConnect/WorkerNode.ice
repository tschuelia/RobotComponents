/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotComponents
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_RRTConnect_WorkerNode_SLICE
#define _ARMARX_INTERFACE_RobotComponents_PlanningAlgorithms_RRTConnect_WorkerNode_SLICE

#include <RobotComponents/interface/components/MotionPlanning/CSpace/CSpace.ice>

#include <RobotComponents/interface/components/MotionPlanning/Tasks/RRTConnect/DataStructures.ice>
#include <RobotComponents/interface/components/MotionPlanning/Tasks/RRTConnect/Task.ice>

module armarx
{
module rrtconnect
{
    class WorkerNodeBase;
    sequence<WorkerNodeBase*> WorkerNodeBasePrxList;

    /**
     * @brief The worker used by RRTConnect.
     */
    ["cpp:virtual"]
    class WorkerNodeBase implements TreeUpdateInterface
    {
        //problem
        /**
         * @brief The used planning cspace.
         */
        CSpaceBase cspace;
        /**
         * @brief The start config.
         */
        VectorXf startCfg;
        /**
         * @brief The goal config.
         */
        VectorXf goalCfg;

        //parameters
        /**
         * @brief The dcd stepsize.
         */
        float DCDStepSize;

        //management
        /**
         * @brief The workers task.
         */
        TaskBase* task;

        /**
         * @brief The update topics prefix
         */
        string topicName;

        long workerId;
        long workerCount;

        //management
        ["cpp:const"] Update getUpdate(long updateSubId);
        void setWorkerNodes(WorkerNodeBasePrxList workers);
    };
};
};
#endif
