#include <RobotComponents/interface/components/GraspingManager/GraspingManagerInterface.ice>
#include <RobotComponents/interface/components/MotionPlanning/CSpace/SimoxCSpace.ice>
#include <ArmarXCore/interface/core/UserException.ice>


module armarx
{
    struct MotionPlanningData
    {
        PoseBase globalPoseStart;
        PoseBase globalPoseGoal;
        NameValueMap configStart;
        NameValueMap configGoal;
        string rnsToUse;
        string endeffector;
    };

    interface PlannedMotionProviderInterface
    {
        GraspingTrajectory planMotion(SimoxCSpaceBase cSpace, SimoxCSpaceBase cspacePlatform, MotionPlanningData mpd) throws RuntimeError;
    };
};
