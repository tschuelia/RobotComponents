#ifndef ARMARX_TRAJECTORYPLAYER_SLICE
#define ARMARX_TRAJECTORYPLAYER_SLICE

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
//    struct TrajSource
//    {
//        DoubleSeqSeqSeq dataVec;
//        Ice::DoubleSeq timestamps;
//        Ice::StringSeq dimensionNames;
//    };


    interface TrajectoryPlayerInterface extends KinematicUnitListener
    {
        bool startTrajectoryPlayer();
        bool pauseTrajectoryPlayer();
        bool stopTrajectoryPlayer();
        bool resetTrajectoryPlayer(bool moveToFrameZeroPose);

//        void load(TrajSource traj, double start, double end, double timestep, ::Ice::StringSeq joints);
//        void load(TrajectoryBase traj, double start, double end, double timestep, ::Ice::StringSeq joints);

        void loadJointTraj(TrajectoryBase jointTraj);
        void loadBasePoseTraj(TrajectoryBase basePoseTraj);

        void setLoopPlayback(bool loop);
        void setIsVelocityControl(bool isVelocity);
        void setIsPreview(bool isPreview);
        bool setJointsInUse(string jointName, bool inUse);
        void enableRobotPoseUnit(bool isRobotPose);

        double getCurrentTime();
        double getEndTime();
        double getTrajEndTime();

        void setEndTime(double endTime);


    };
};

#endif
